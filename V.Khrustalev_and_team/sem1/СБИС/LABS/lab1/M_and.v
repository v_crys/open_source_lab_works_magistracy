
module M_and(
	input X1,
	input X2,

	output out
);

	assign out = X1 & X2;

endmodule