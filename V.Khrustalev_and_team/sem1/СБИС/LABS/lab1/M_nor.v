
module M_nor(
	input IN,

	output OUT);

	assign OUT = ~IN;

endmodule