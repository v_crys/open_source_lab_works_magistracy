
module M_tb();

	reg [3 : 0 ] in = 0;

	wire out;

	reg clk = 0;

	wire test = (in[3] & in[2]) | (~in[1]) | (~in[0]); 

	always
	begin
		#10;
		clk = ~clk;
	end

	always @(posedge clk)
	begin
		in <= in + 1;

		if ( test != out )
		begin
			$display( "error" );
			$stop;
		end
	end
	

	main main( in[3], in[2], in[1], in[0], out );

endmodule