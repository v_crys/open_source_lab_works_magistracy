
module main(
	input A,
	input B,
	input C,
	input D,

	output OUT);


	wire nor_C;
	wire nor_D;
	wire A_and_B;
	wire or1;	


	M_nor M_nor1 ( .IN(C), .OUT(nor_C) );
	M_nor M_nor2 ( .IN(D), .OUT(nor_D) );
	
	M_and M_and1 ( A, B, A_and_B );

	M_or M_or1 ( A_and_B, nor_C, or1 );

	M_or M_or2 ( or1, nor_D, OUT );
	

endmodule