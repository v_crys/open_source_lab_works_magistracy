`timescale 1ns / 1ps

module TB(

    );
    
    	parameter CNT_DEV = 10;
    
    reg clk = 0;
	reg rst;
	reg en;

	reg [CNT_DEV - 1 : 0] IN_DEV;
	reg [2:0] IN_PRIOR [(CNT_DEV - 1) : 0];

	wire [CNT_DEV - 1 : 0] OUT_DEV;
    
    integer ii;
    integer st_mh;
    integer cnt;
    arbitr #(CNT_DEV) arbitr  ( clk, rst, en, IN_DEV, IN_PRIOR, OUT_DEV );
    
    initial
    begin
        rst = 0;
        #10;
        rst = 1;
        en = 0;
        
        cnt = 0;
        st_mh = 0;
        
        for (ii = 0; ii < CNT_DEV; ii = ii + 1 )
        begin
            IN_PRIOR[ ii ] = (ii**2) % 5;
            IN_DEV[ ii ] = 0;
        end
    end
    
    
    always
    begin
        #10;
        clk = ~clk;
    end
    
    always @(posedge clk)
    begin
        case (st_mh )
            'd0:
                begin
                    en <= 1;
                    IN_DEV <= 1 << cnt;
                    st_mh <= 1;
                end
                
            'd1:
                begin
                    en <= 0;
                    st_mh <= 2;
                end
                
            'd2:
                begin
                    st_mh <= 'd3;
                end
                
            'd3:
                begin
                    if ( IN_DEV != OUT_DEV )
                    begin
                        $display( "ERROR" );
                        $stop;
                    end
                    st_mh <= 0;
                    cnt <= cnt + 1;
                    
                    if ( cnt > CNT_DEV ) 
                        $stop;
                end
            
        endcase
        
    end
    
    
endmodule
