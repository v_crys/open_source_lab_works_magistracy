`timescale 1ns / 1ps
///////////////////////////////////////

module arbitr
#(parameter CNT_DEV = 5)
( clk, rst, en, IN_DEV, IN_PRIOR, OUT_DEV );

	

	input clk;
	input rst;
	input en;

	input [CNT_DEV - 1 : 0] IN_DEV;
	input [2:0] IN_PRIOR [(CNT_DEV - 1) : 0];

	output reg [CNT_DEV - 1 : 0] OUT_DEV;

	
	

	reg [ 7 : 0 ] last_dev [4:0];
	reg [ 7 : 0 ] ii = 0; 
	reg [ 7 : 0 ] jj = 0;
	reg [ 7 : 0 ] st_mh = 0;
	
	reg [ 2 : 0 ] dev_prior; 
	
	reg	[CNT_DEV - 1 : 0] IN_DEV_buf;
	//reg [7:0] PRIOR_ARR [(CNT_DEV - 1) : 0];

	always @( posedge clk, negedge rst )
	begin
		if ( ~rst )
		begin
		    for (ii = 0; ii < 5; ii = ii + 1)
			     last_dev[ ii ] <= 0;
			//ii <= 0;
			st_mh <= 0;
			dev_prior <= 0;

		end else
		begin
			case (st_mh)
				'd0:
					begin
						if (en == 1)
						begin
							IN_DEV_buf <= IN_DEV;
							
							for ( ii = 0; ii < CNT_DEV; ii = ii+ 1)
							begin
								if ( ( IN_DEV[ ii ] == 1 ) && ( IN_PRIOR[ ii ] > dev_prior ) )
								begin
									dev_prior <= IN_PRIOR[ ii ];
								end
							end
							
							st_mh <= 1;
						end
					end
			
			     'd1:
			         begin
                            dev_prior <= 0;
                            st_mh <= 0;
                            
                         OUT_DEV <= 0;
			             for ( ii = 0; ii < CNT_DEV; ii = ii + 1 )
			             begin
			                 if (( IN_DEV_buf[ (ii + last_dev[dev_prior] + 1) % CNT_DEV ] == 1 ) && 
			                     ( IN_PRIOR[ (ii + last_dev[dev_prior] + 1) % CNT_DEV ] == dev_prior ))
			                 begin
			                     OUT_DEV <= 1 << ((ii + last_dev[dev_prior] + 1) % CNT_DEV);
			                     last_dev[ dev_prior ] <= ((ii + last_dev[dev_prior] + 1) % CNT_DEV);
			                     break;
			                 end
			                     
			             end
			         
			         end
			endcase
		end
	end


endmodule