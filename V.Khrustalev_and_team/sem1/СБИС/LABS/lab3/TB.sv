module TB();

parameter	MEM_WORD_SIZE	=	32;
parameter	CNT_INTERFACE	=	4;
parameter 	MEM_SIZE	=	30;


	logic	clk;
	logic	rstn;

	logic	bus_en 	[CNT_INTERFACE - 1 : 0];
	logic	[MEM_WORD_SIZE - 1 : 0] bus_wdata [CNT_INTERFACE - 1 : 0];
	logic	[MEM_WORD_SIZE - 1 : 0] bus_addr  [CNT_INTERFACE - 1 : 0];
	logic	bus_write	[CNT_INTERFACE - 1 : 0];
	logic	bus_ready 	[CNT_INTERFACE - 1 : 0];
	logic	bus_err		[CNT_INTERFACE - 1 : 0];
	logic	[MEM_WORD_SIZE - 1 : 0] bus_rdata [CNT_INTERFACE - 1 : 0];

	

	logic	[7 : 0] st_mh [3 : 0];
	int i;	

	int fd [3 : 0];


	initial
	begin
		fd[0] = $fopen( "/home/1940/khrustalev/LABS/lab3/test.txt", "r");
		fd[1] = $fopen( "/home/1940/khrustalev/LABS/lab3/test2.txt", "r");
		fd[2] = $fopen( "/home/1940/khrustalev/LABS/lab3/test3.txt", "r");
		fd[3] = $fopen( "/home/1940/khrustalev/LABS/lab3/test4.txt", "r");
		
		rstn = 0;
		#5;
		rstn = 1;
		
		clk = 0;
		st_mh[0] <= 0;
		st_mh[1] <= 0;
		st_mh[2] <= 0;
		st_mh[3] <= 0;
	end

	always
	begin
		#10;
		clk = ~clk;
	end

	
	

	always @(posedge clk or negedge rstn)
	begin
		if (~rstn)
		begin
			for (i = 0; i < CNT_INTERFACE; i = i + 1)
			begin
				bus_en[ i ] <= 'd0;
				bus_wdata[i] <= 'd0;
				bus_addr[i] <= 'd0;
				bus_write[i] <= 1'b0;
			end
		end else
		begin
		for ( i = 0; i <CNT_INTERFACE; i = i + 1)
		begin
			if (fd[ i ] == 0)
				$display( "File not found" );

			case (st_mh[ i ])
				0:
				begin
					// load from file transaction
					if ($feof(fd[i]))
					begin
						$display( "Success" );
						$stop;
					end

					$fscanf( fd[i], "%h\t%h\t%h", bus_write[ i ], bus_addr[ i ], bus_wdata[ i ] );
					bus_en[i] <= 'd1;

					st_mh[ i ] <= 'd1;
				end

				1:
				begin
					bus_en[ i ] <= 'd0;
					if (bus_ready[ i ])
					begin
						if (bus_err[i]) 
						begin
							$display( "Error" );
							$stop;
						end

						if ( !bus_write[ i ] )
						begin
							if (bus_wdata[ i ] != bus_rdata[ i ])
							begin
								$display( "Data not correct");
								$stop;

							end
						end

						st_mh[ i ] <= 0;
					end

					

				end

			
			endcase
		end
		end

	end



reg_map
#(
	.MEM_WORD_SIZE (MEM_WORD_SIZE),
	.CNT_INTERFACE (CNT_INTERFACE),
	.MEM_SIZE (MEM_SIZE)
) 
reg_map
(
	.clk 	(clk		),
	.rstn 	(rstn		),

	.bus_en (bus_en		),
	.bus_wdata 	(bus_wdata	),
	.bus_addr 	(bus_addr	),
	.bus_write	(bus_write	),
	.bus_ready 	(bus_ready	),
	.bus_err	(bus_err	),
	.bus_rdata 	(bus_rdata	)

);


endmodule