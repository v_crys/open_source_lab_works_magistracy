
module alu_tb();

parameter SIZE_WORD = 4;
parameter TYPE_SUM = 1;
parameter TYPE_SUB = 2;

logic   clk;
logic   rstn;

logic   [SIZE_WORD - 1 : 0] op1;
logic   [SIZE_WORD - 1 : 0] op2;
    
logic   [7 : 0]             code;
logic   [SIZE_WORD - 1 : 0] res;

initial
begin
    rstn = 0;
    #5;
    rstn = 1;
    
    clk = 0;
    
    forever #5 clk = ~clk;
end

always_ff @(posedge clk)
begin
    case (code)
        'd0: if ( res != (op1 + op2) ) begin $error( "error sum" ); $stop; end
        'd1: if ( res != (op1 - op2) ) begin $error( "error sub" ); $stop; end
        'd2: if ( res != (~op1 + 1'b1 ) ) begin $error( "error -" ); $stop; end
        'd3: if ( res != (op1 <<< op2) ) begin $error( "error shift" ); $stop; end
        'd4: if ( res != (op1 ^ op2) )  begin $error( "error xor" ); $stop; end
        'd5: if ( res != (op1 * op2) )  begin $error( "error mul" ); $stop; end
        'd6: if ( res != (~op1) )       begin $error( "error nor" ); $stop; end
        'd7: if ( res != (op1 + 1'b1) ) begin $error( "error inc" ); $stop; end
        'd8: if ( res != (op1 - 1'b1) ) begin $error( "error dec" ); $stop; end
    endcase
end


always_ff @(posedge clk, negedge rstn)
begin
    if (~rstn)
        code    <= 'd0;
    else 
        code    <= (code < 'd8) ? (code + 'd1) : 'd0;
        
    op1 <= $random();
    op2 <= $random();
end

ALU
#(
    .SIZE_WORD (SIZE_WORD),
    .TYPE_SUM  (TYPE_SUM),
    .TYPE_SUB  (TYPE_SUB)
)
ALU
(
    .op1 (op1),
    .op2 (op2),
    
    .code(code),
    
    .res(res)
);

endmodule