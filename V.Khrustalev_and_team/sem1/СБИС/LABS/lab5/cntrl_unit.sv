//------------------------------------------
// Control unit. Unit download program from file, and confirm result
//------------------------------------------

module ctrl_unit();
	parameter 	START_ADDR	= 0;
	parameter 	SOFT_PATH 	= "./prog/1.dat";
    
    parameter	MEM_WORD_SIZE	=	32;
    parameter	CNT_INTERFACE	=	3;  
    parameter 	MEM_SIZE	    =	512;
    
    parameter   ADDR_WORD_RES   =   'hFF;
    parameter   SOFT_RESULT     =   'hFFFF_FF56;   ///< not use zero value

	logic	clk;
	logic 	rstn;
	logic   rstn_cpu;
    
    int     fd;
	
    logic	                        bus_en_cntl;
	logic	[MEM_WORD_SIZE - 1 : 0] bus_wdata_cntl;
	logic	[MEM_WORD_SIZE - 1 : 0] bus_addr_cntl;
	logic	                        bus_write_cntl;
	logic	                        bus_ready_ctrl;
	logic	                        bus_err_ctrl;
	logic	[MEM_WORD_SIZE - 1 : 0] bus_rdata_ctrl;
    
    logic	                        bus_en 	    [CNT_INTERFACE - 2 : 0];
	logic	[MEM_WORD_SIZE - 1 : 0] bus_wdata   [CNT_INTERFACE - 2 : 0];
	logic	[MEM_WORD_SIZE - 1 : 0] bus_addr    [CNT_INTERFACE - 2 : 0];
	logic	                        bus_write	[CNT_INTERFACE - 2 : 0];
	logic	                        bus_ready 	[CNT_INTERFACE - 2 : 0];
	logic	                        bus_err		[CNT_INTERFACE - 2 : 0];
	logic	[MEM_WORD_SIZE - 1 : 0] bus_rdata   [CNT_INTERFACE - 2 : 0];
	
	initial
	begin
    
		rstn	= 0;
		#5;
		rstn 	= 1;
		
		
		clk = 0;
		
		forever #10 clk = ~clk;
	end
    
    typedef enum logic [7 : 0] {
        LOAD_FILE,
        CALC,
        TEST_RES
    } fsm_type;
    
    fsm_type    fsm;
	
    logic   [15 : 0]    f_addr;
    logic   [MEM_WORD_SIZE - 1: 0]    f_data;
    
    logic   [7 : 0]     fsm_sub;
    
    logic   [MEM_WORD_SIZE - 1 : 0] buf_res;
    
    
	always_ff @(posedge clk, negedge rstn)
	begin
		if (~rstn)
		begin
			fd  = $fopen( SOFT_PATH, "r" );
            
            if (!fd)
            begin
                $display( "file with program not found" );
                $stop;
            end
            
            fsm <= LOAD_FILE;
            fsm_sub <= 'd0;
            
            rstn_cpu <= 0;
		end else
		begin
            case (fsm)
                LOAD_FILE:
                begin
                    case (fsm_sub)
                        'd0:
                        begin
                            
                    
                            if ($feof(fd))
                            begin
                                fsm <= CALC;
                            end else
                            begin
                                $fscanf( fd, "%h\t%h\n", f_addr, f_data );
                                
                                bus_en_cntl     <= 1'b1;
                                bus_write_cntl  <= 1'b1;
                                bus_addr_cntl   <= f_addr;
                                bus_wdata_cntl  <= f_data;
                                
                                fsm_sub <= 1'b1;
                            end
                            
                        end
                        
                        'd1:
                        begin
                            bus_en_cntl     <= 1'b0;
                            if (bus_ready_ctrl)
                            begin
                                if (bus_err_ctrl)
                                begin
                                    $error( "Error write memory from file" );
                                    $stop;                                
                                end
                                
                                
                                fsm_sub <= 'd0;
                            end
                        end
                    endcase
                        
                        
                end
                
                CALC:
                begin
                    rstn_cpu    <= 'd1;
                    
                    case (fsm_sub)
                        'd0:
                        begin
                            bus_en_cntl     <= 1'b1;
                            bus_addr_cntl   <= ADDR_WORD_RES;
                            bus_write_cntl  <= 1'b0;
                            
                            fsm_sub <= 'd1;
                        end
                        
                        'd1:
                        begin
                            bus_en_cntl = 'd0;
                            if (bus_ready_ctrl)
                            begin
                                fsm_sub <= 'd0;
                                
                                if (bus_rdata_ctrl)
                                begin
                                    buf_res <= bus_rdata_ctrl;
                                    fsm <= TEST_RES;
                                    rstn_cpu    <= 'd0;
                                end
                            end
                        end
                    endcase 
                end
                
                TEST_RES:
                begin
                    if ( buf_res == SOFT_RESULT )
                        $display( "Test success" );
                    else
                        $display( "Test error" );
                        
                    $stop;
                end
            endcase
		end
	end
	
memory_unit
#(
    .MEM_WORD_SIZE (MEM_WORD_SIZE),
    .CNT_INTERFACE (CNT_INTERFACE),
    .MEM_SIZE	   (MEM_SIZE)
)
memory_unit 
(
	.clk,
	.rstn,

    .bus_en         ( '{ bus_en[0], bus_en[1], bus_en_cntl})  ,
    .bus_wdata      ( '{ bus_wdata[0], bus_wdata[ 1 ], bus_wdata_cntl} ),
    .bus_addr       ( '{ bus_addr[ 0 ], bus_addr[ 1 ], bus_addr_cntl} ),
    .bus_write      ( '{ bus_write[ 0 ], bus_write[ 1 ], bus_write_cntl} ),
    .bus_ready      ( '{ bus_ready[ 0 ], bus_ready[ 1 ], bus_ready_ctrl} ),
    .bus_err        ( '{ bus_err[ 0 ], bus_err[ 1 ], bus_err_ctrl} ),
    .bus_rdata      ( '{ bus_rdata[ 0 ], bus_rdata[ 1 ], bus_rdata_ctrl} )
);


cpu_top
#(
    .MEM_WORD_SIZE (MEM_WORD_SIZE)
)
cpu_top
(
    .clk    (clk        ),
    .rstn   (rstn_cpu   ),


    //     output memory interface
    .bus_en         ( bus_en        ),
    .bus_wdata      ( bus_wdata     ),
    .bus_addr       ( bus_addr      ),
    .bus_write      ( bus_write     ),
    .bus_ready      ( bus_ready     ),
    .bus_err        ( bus_err       ),
    .bus_rdata      ( bus_rdata     )
);
	

endmodule