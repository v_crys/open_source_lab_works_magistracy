//---------------------------------
// ALU Unit
//---------------------------------

module alu_unit
#(
    parameter SIZE_WORD = 4,
    parameter TYPE_SUM = 0,
    parameter TYPE_SUB = 0
)
(
    input   logic   [SIZE_WORD - 1 : 0] op1,
    input   logic   [SIZE_WORD - 1 : 0] op2,
    
    input   logic   [7 : 0]             code,
    
    output  logic   [SIZE_WORD - 1 : 0] res,
    output  logic                       z      ///< zero
);

function [1 : 0] simple_sum
    (
        input logic a,
        input logic b,
        input logic c
    );
    return a + b + c;
    
endfunction

class calc_sum 
#(
    parameter int SIZE_SUM_S=8,
    parameter int SIZE_SUM_P=8    
);
    static function [SIZE_SUM_P - 1 : 0] sum_parallel
    (
        input logic     [SIZE_SUM_P - 1 : 0]     op1,
        input logic     [SIZE_SUM_P - 1 : 0]     op2,
        input logic                              c
    );      
        begin
            int idx;
            logic   [SIZE_SUM_P : 0]         carry;
            logic   [SIZE_SUM_P - 1 : 0]     res;
        
            carry[ 0 ] = c;
            for ( idx = 0; idx < SIZE_SUM_P; idx++ )
            begin
                carry[ idx+1 ] = (op1[ idx ] & op2[ idx ])    | 
                                 (op1[ idx ] & carry[ idx ])  |
                                 (op2[ idx ] & carry[ idx ]);
               
                res[idx] = op1[idx] ^ op2[idx] ^ carry[ idx ];
            end
    
            return res;
        end
    
    endfunction
    
    static function [SIZE_SUM_S - 1 : 0] sum_serial
    (
        input logic     [SIZE_SUM_S - 1 : 0] op1,
        input logic     [SIZE_SUM_S - 1 : 0] op2,
        input logic                         c
    );      
        begin
            int idx;
            logic   [SIZE_SUM_S - 1 : 0]    res;
            logic   [SIZE_SUM_S : 0]        carry;
            
            carry[ 0 ] = c;
            for ( idx = 0; idx < SIZE_SUM_S; idx++ )
            begin
                {carry[ idx + 1 ], res[ idx ]} = simple_sum( op1[ idx ] , op2[ idx ] , carry[ idx ]);   
            end
            
            return res;
        end
    
    endfunction
endclass

// sum 
logic   [SIZE_WORD - 1 : 0] sum_res;
generate
    if ( TYPE_SUM == 0 )
        assign sum_res  = op1 + op2;
    else if (TYPE_SUM == 1)
        assign sum_res     = calc_sum #(SIZE_WORD, SIZE_WORD)::sum_serial(op1, op2, 1'b0);
    else
        assign sum_res     = calc_sum #(SIZE_WORD, SIZE_WORD)::sum_parallel( op1, op2, 1'b0 ); 
endgenerate

// sub
logic   [SIZE_WORD - 1 : 0] sub_res;
generate
    if ( TYPE_SUM == 0 )
        assign sub_res  = op1 - op2;
    else if (TYPE_SUM == 1)
        assign sub_res     = calc_sum #(SIZE_WORD, SIZE_WORD)::sum_serial(op1, ~op2, 1'b1);
    else 
        assign sub_res     = calc_sum #(SIZE_WORD, SIZE_WORD)::sum_parallel( op1, ~op2, 1'b1 ); 
endgenerate

always_comb
begin
    case (code)
        'd0:     
        begin
            // sum 
            res = sum_res;
        end
        
        'd1:
        begin
            // sub
            res     = sub_res;
        end
        
        'd2:
        begin
            // mul -1
            res = ~op1 + 1'b1;
        end
        
        'd3:
        begin
            // round shift 
            res = (op1 <<< op2);
        end
        
        'd4:
        begin
            // xor
            res = op1 ^ op2;
        end
        
        'd5:
        begin
            res = op1 * op2;
        end
        
        'd6:
        begin
            // not
            res = ~op1;
        end
        
        'd7:
        begin
            res = op1 + 1'b1;
        end
        
        'd8:
        begin
            res = op1 - 1'b1;
        end
        
        
    
    endcase
    
    z = (res == 'd0);
end
endmodule
