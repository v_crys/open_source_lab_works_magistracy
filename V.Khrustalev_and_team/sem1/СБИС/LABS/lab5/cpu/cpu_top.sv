//-------------------------------------
// CPU TOP
//-------------------------------------

module    cpu_top
#(
    parameter MEM_WORD_SIZE = 32
)
(
    input    logic    clk,
    input    logic    rstn,


    //     output memory interface
    output  logic                            bus_en     [1 : 0],
    output  logic    [MEM_WORD_SIZE - 1 : 0] bus_wdata  [1 : 0],
    output  logic    [MEM_WORD_SIZE - 1 : 0] bus_addr   [1 : 0],
    output  logic                            bus_write  [1 : 0],
    input   logic                            bus_ready  [1 : 0],
    input   logic                            bus_err    [1 : 0],
    input   logic    [MEM_WORD_SIZE - 1 : 0] bus_rdata  [1 : 0]
);

    
    logic   [MEM_WORD_SIZE - 1 : 0] op1;
    logic   [MEM_WORD_SIZE - 1 : 0] op2;
    
    logic   [7 : 0]             code;
    
    logic   [MEM_WORD_SIZE - 1 : 0] res;

    alu_unit
    #(
        .SIZE_WORD (MEM_WORD_SIZE) 
    ) alu_unit (
            .op1        (op1        ),
            .op2        (op2        ),
            .code       (code       ),
    
            .res        (res        )
    );
    
    
    mem_intf_unit
    #(
        .MEM_WORD_SIZE (MEM_WORD_SIZE)
    )
    mem_intf_unit
    (
        .clk        (clk            ),
        .rstn       (rstn           ),

        //     memory interface
        .bus_en     (bus_en         ),
        .bus_wdata  (bus_wdata      ),
        .bus_addr   (bus_addr       ),
        .bus_write  (bus_write      ),
        .bus_ready  (bus_ready      ),
        .bus_err    (bus_err        ),
        .bus_rdata  (bus_rdata      ),    
    
        //     ALU
        .alu_op1    (op1            ),
        .alu_op2    (op2            ),
        .alu_code   (code           ),
        
        .alu_res    (res            )
);


endmodule