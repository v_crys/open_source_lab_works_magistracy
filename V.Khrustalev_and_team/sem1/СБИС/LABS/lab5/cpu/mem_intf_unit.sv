//--------------------------------
// Memory interface unit
//--------------------------------

module mem_intf_unit
#(
    parameter MEM_WORD_SIZE = 32
)
(
	input	logic	clk,
	input 	logic	rstn,

	
	// 	memory interface cmd
	output	logic							bus_en      [1 : 0],
	output 	logic	[MEM_WORD_SIZE - 1 : 0] bus_wdata   [1 : 0],
	output 	logic	[MEM_WORD_SIZE - 1 : 0] bus_addr    [1 : 0],
	output 	logic							bus_write   [1 : 0],
	input 	logic							bus_ready   [1 : 0],
	input   logic							bus_err     [1 : 0],
	input   logic	[MEM_WORD_SIZE - 1 : 0] bus_rdata   [1 : 0],	
	
	// 	ALU
	output	logic	[MEM_WORD_SIZE - 1 : 0]	alu_op1,
	output	logic	[MEM_WORD_SIZE - 1 : 0]	alu_op2,
    output  logic   [7 : 0]                 alu_code,
	input	logic	[MEM_WORD_SIZE - 1 : 0]	alu_res
);


	logic	[15 : 0]	PC;
	
	typedef enum logic [7 : 0] {
        LOAD_CMD,
        DEC_CMD,
        WR_RES
    
    } fsm_type;
	
    fsm_type        fsm;
    
    
    logic       [MEM_WORD_SIZE - 1 : 0]     command;
    
    logic tmp_flag;
    
    always_ff @(posedge clk, negedge rstn)
    begin
        if (~rstn)
        begin
            PC  <= 'd0;
            fsm <= LOAD_CMD;
            
            bus_en[ 0 ] <= 'd0;
            bus_en[ 1 ] <= 'd0;
        end else
        begin
            case (fsm)
                LOAD_CMD:
                begin
                    bus_en[ 0 ]      <= 1'b1;
                    bus_addr[ 0 ]    <= PC;
                    bus_write[ 0 ]   <= 1'b0;
                            
                                           
                    if (bus_en[ 0 ] & bus_ready[ 0 ])
                    begin
                        if (bus_err[ 0 ])
                        begin
                            $display( "Error in mem intf" );
                            $stop;
                        end
                        
                        bus_en[ 0 ]  <= 1'b0;
                        command <= bus_rdata[ 0 ];
                        fsm <=  DEC_CMD;
                    end
                                               
                       
                end
                
                DEC_CMD:
                begin
                    alu_code    <= '{command[ MEM_WORD_SIZE - 1 : MEM_WORD_SIZE - 4 ]};
                    
                    bus_en[ 0 ]     <= 1'b1;
                    bus_write[ 0 ]  <= 1'b0;
                    bus_addr[ 0 ]   <= command[ MEM_WORD_SIZE - 5 : (MEM_WORD_SIZE - 5) * 2 / 3 ];
                    
                    bus_en[ 1 ]     <= 1'b1;
                    bus_write[ 1 ]  <= 1'b0;
                    bus_addr[ 1 ]   <= command[ (MEM_WORD_SIZE - 5) * 2 / 3 - 1 : (MEM_WORD_SIZE - 5) / 3 ];
                    
                    
                    if ( bus_ready[ 0 ] && bus_ready[ 1 ] )
                    begin
                        if (bus_err[ 0 ] || bus_err[ 1 ])
                        begin
                            $display( "error in input data cmd" );
                            $stop;
                        end
                        
                        alu_op1 <= '{bus_rdata[ 0 ]};
                        alu_op2 <= '{bus_rdata[ 1 ]};
                        
                        bus_en[ 0 ] <= 1'b0;
                        bus_en[ 1 ] <= 1'b0;
                        fsm <= WR_RES;
                        tmp_flag <= 1;
                    end
                end
                
                WR_RES:
                begin
                    
                    bus_en[ 1 ] <= 1'b1;
                        
                    bus_write[ 1 ] <= 1'b1;
                    bus_addr[ 1 ] <= command[ (MEM_WORD_SIZE - 5) / 3 - 1 : 0 ];
                    bus_wdata[ 1 ] <= alu_res;
                    
                    if (bus_ready[ 1 ])
                    begin
                        if (bus_err[ 1 ])
                        begin
                            $display( "Error write result" );
                            $stop;
                        end
                        
                        bus_en[ 1 ] <= 1'b0;
                        fsm <= LOAD_CMD;
                        PC  <= PC + 1;
                    end
                end
            
            endcase
        end
    
    end


endmodule