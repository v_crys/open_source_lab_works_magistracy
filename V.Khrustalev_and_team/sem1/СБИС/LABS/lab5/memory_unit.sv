//-----------------------------
// External memory for program and data
//-----------------------------

module memory_unit
#(
parameter	MEM_WORD_SIZE	=	32,
parameter	CNT_INTERFACE	=	4,
parameter 	MEM_SIZE	    =	30
) 
(
	input 	logic	clk,
	input	logic	rstn,

	input	logic	bus_en 	[CNT_INTERFACE - 1 : 0],
	input	logic	[MEM_WORD_SIZE - 1 : 0] bus_wdata [CNT_INTERFACE - 1 : 0],
	input	logic	[MEM_WORD_SIZE - 1 : 0] bus_addr  [CNT_INTERFACE - 1 : 0],
	input	logic	bus_write	[CNT_INTERFACE - 1 : 0],
	output 	logic	bus_ready 	[CNT_INTERFACE - 1 : 0],
	output	logic	bus_err		[CNT_INTERFACE - 1 : 0],
	output	logic	[MEM_WORD_SIZE - 1 : 0] bus_rdata [CNT_INTERFACE - 1 : 0]
);


	logic	[7 : 0] fsm	[ CNT_INTERFACE - 1 : 0 ];
	
	logic	[7 : 0] ii, jj;

	logic [MEM_WORD_SIZE - 1 : 0] reg_mem [ MEM_SIZE - 1 : 0];
	


	always @( posedge clk or negedge rstn )
	begin
		if (~rstn)
		begin
			for ( ii = 0; ii < CNT_INTERFACE; ii = ii + 1 )
				fsm[ ii ]	<= 0;
		end else
		begin
			for ( ii = 0; ii < CNT_INTERFACE; ii = ii + 1 )
			begin
				case ( fsm[ ii ] )
					0:
					begin
						bus_err[ ii ] <= 0;
						bus_ready[ ii ] <= 0;
						bus_rdata[ ii ] <= 0;	
						if ( bus_en[ ii ] )
						begin
							fsm[ ii ] <= 1;
							
							if ( bus_addr[ ii ] >= MEM_SIZE )
							begin
								bus_err[ ii ] <= 1;
								bus_ready[ ii ] <= 1;
								fsm[ ii ] <= 0;
							end
						end 	
					end

					1:
					begin
					   
					
						if (bus_write[ ii ])
						begin
							bus_err[ ii ] <= 0;

							for (jj = ii + 1; jj < CNT_INTERFACE; jj = jj + 1 )
								if ( ( bus_addr[ ii ] == bus_addr[ jj ] ) && (bus_write[ jj ]) )
								begin
									bus_err[ ii ] <= 1;
									break;
								end
							
							// write
							reg_mem[ bus_addr[ ii ] ] <= bus_wdata[ ii ];
							bus_ready[ ii ] <= 1;
							

						end else
						begin
							//read
							bus_rdata[ ii ] <= reg_mem[ bus_addr[ ii ] ];
							bus_ready[ ii ] <= 1;
							bus_err[ ii ] <= 0;
						end
						
						fsm[ii] <= 2;
					end
					
					2:
					begin
					   bus_ready[ ii ] <= 0;
					   //if (!bus_en[ ii ])
    						fsm[ ii ] <= 0;
					end
				endcase
			end
		end

	end


endmodule