import numpy as np
from math import sqrt, atan2, cos, sin
from matplotlib.pyplot import imread, imsave, cm
from scipy.spatial.distance import cdist


# вспомогательные классы и функции
class Point:
    def __init__(self, x_init, y_init):
        self.x = x_init
        self.y = y_init

    def shift(self, x, y):
        self.x += x
        self.y += y


# Рассчет центра тяжести
def centre_weight(img):
    Txy = np.sum(img)
    pt = Point(0, 0);

    for x in range(img.shape[1]):
        for y in range(img.shape[0]):
            pt.x += x * img[y, x]
            pt.y += y * img[y, x]

    pt.x //= Txy
    pt.y //= Txy
    return pt


# Пересчет координат с учетом того, что центр координат должен быть совмещен с центром тяжести
def calc_coord_by_CW(pt, pt_c):
    pt_ret = Point(0, 0)
    pt_ret.x = pt.x - pt_c.x
    pt_ret.y = pt.y - pt_c.y
    return pt_ret


def move_coord_to_center(img, coord_arr, pt_c):
    for x in range(img.shape[1]):
        for y in range(img.shape[0]):
            pt = Point(x, y);
            pt_centered = calc_coord_by_CW(pt, pt_c)
            coord_arr[x][y] = pt_centered;
    return coord_arr


# Рассчет направления и величины сжатия изображения
def calc_mu_and_theta(img, coord_arr):
    b = 0.0
    c = 0.0
    d = 0.0
    for x in range(img.shape[1]):
        for y in range(img.shape[0]):
            b += (coord_arr[x][y].x ** 2 - coord_arr[x][y].y ** 2) * img[y, x]
            d += (coord_arr[x][y].x ** 2 + coord_arr[x][y].y ** 2) * img[y, x]
            c += 2 * coord_arr[x][y].x * coord_arr[x][y].y * img[y, x]

    theta = atan2(c, b) / 2
    mu = sqrt((d + sqrt(b ** 2 + c ** 2)) / (d - sqrt(b ** 2 + c ** 2)))

    return theta, mu


# Компенсация
def compensation_A3(img, coord_arr, theta, mu):
    for x in range(img.shape[1]):
        for y in range(img.shape[0]):
            a1 = (coord_arr[x][y].x * cos(-theta) - coord_arr[x][y].y * sin(-theta)) / mu
            a2 = coord_arr[x][y].x * sin(-theta) + coord_arr[x][y].y * cos(-theta)
            coord_arr[x][y].x = a1 * cos(theta) - a2 * sin(theta)
            coord_arr[x][y].y = a1 * sin(theta) + a2 * cos(theta)
    return coord_arr


# Вычисление коэффициента равномерного масштабирования М
def calc_M(img, coord_arr):
    M = 0.0
    K = 10.0  # эталонное значение центра тяжести проекции
    for x in range(img.shape[1]):
        for y in range(img.shape[0]):
            M += sqrt(coord_arr[x][y].x ** 2.0 + coord_arr[x][y].y ** 2.0) * img[y, x]
    M /= (K * np.sum(img))
    return M


# Компенсация A5
def compensation_A5(img, coord_arr, M):
    for x in range(img.shape[1]):
        for y in range(img.shape[0]):
            coord_arr[x][y].x /= M
            coord_arr[x][y].y /= M
    return coord_arr


# Нормализация
def normalized(img):
    # Для хранения значений новых координат используем двумерный список координатных точек
    coord_arr = []
    for x in range(img.shape[1]):
        coord_arr.append([])
        for y in range(img.shape[0]):
            coord_arr[x].append(Point)

    # Рассчет центра тяжести для компенсации A1, A2
    pt_c = centre_weight(img)

    # Перемесщение координат в центр
    coord_arr = move_coord_to_center(img, coord_arr, pt_c)

    # Рассчет theta и mu для компенсации A3
    theta, mu = calc_mu_and_theta(img, coord_arr)

    # Компенсация A3
    coord_arr = compensation_A3(img, coord_arr, theta, mu)

    # Компенсация A5
    M = calc_M(img, coord_arr)
    coord_arr = compensation_A5(img, coord_arr, M)

    # Формирование результирующего изображения путем перемещения пикселей
    res = np.zeros((img.shape[1], img.shape[0]))
    for x in range(img.shape[1]):
        for y in range(img.shape[0]):
            x_new = coord_arr[x][y].x + pt_c.x
            y_new = coord_arr[x][y].y + pt_c.y
            x_new = x_new.astype(int)
            y_new = y_new.astype(int)
            res[y_new, x_new] = img[y, x]

    return res


filenames = ['../image_1_10.pcx', '../image_2_10.pcx', '../image_3_10.pcx', '../my.pcx']
number_research_img = 2


def main():
    afiles = []
    dist = []

    # Шаг 1. Нормализация изображений
    for i in range(len(filenames)):
        img = imread(filenames[i])
        norm_img = normalized(img)
        imsave(arr=norm_img, fname='norm_img_' + str(i + 1) + '.png', cmap=cm.gray)
        afiles.append(np.fft.fft(norm_img)) # Вычисление преобразования Фурье для A6 (инв. к вращению) и добавление в массив

    for i in range(len(afiles)):
        afiles[i] = abs(afiles[i])

    # Вычисление расстояний между обработанными изображениями (векторами признаков)
    for i in range(len(afiles)):
        dist.append(np.linalg.norm(afiles[i] - afiles[number_research_img], ord=2))
        print("Dist ", number_research_img, " and ", dist[i])

    # Принятие решения
    numb_research = 0
    dist_val = 99999999;
    for i in range(len(afiles)):
        if i == number_research_img:
            continue
        if dist_val > dist[i]:
            dist_val = dist[i]
            numb_research = i

    print("Accepted decision: ", numb_research)

main()
