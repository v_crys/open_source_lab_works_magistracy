import numpy as np
from math import sqrt, atan2, cos, sin, pi
from matplotlib.pyplot import imread, imsave, cm
from scipy.spatial.distance import cdist

filenames = ['../image_1_10.pcx', '../image_2_10.pcx', '../image_3_10.pcx', '../my.pcx']

def affine(img):
    Txy = np.sum(img)
    (i_n, j_n) = img.shape[:2]
    x = np.zeros((i_n, j_n))
    y = np.zeros((i_n, j_n))
    res = np.zeros((j_n, i_n))
    ic = jc = b = c = d = m = 0
    for i in range(i_n):
        for j in range(j_n):
            ic += i * img[j, i]
            jc += j * img[j, i]

    ic //= Txy
    jc //= Txy

    for i in range(i_n):
        for j in range(j_n):
            x[i, j] = i - ic
            y[i, j] = j - jc
            b += (x[i, j] ** 2 - y[i, j] ** 2) * img[j, i]
            c += 2 * x[i, j] * y[i, j] * img[j, i]
            d += (x[i, j] ** 2 + y[i, j] ** 2) * img[j, i]

    theta = atan2(c, b) / 2
    mu = sqrt((d + sqrt(b ** 2 + c ** 2)) / (d - sqrt(b ** 2 + c ** 2)))

    for i in range(i_n):
        for j in range(j_n):
            a1 = (x[i, j] * cos(-theta) - y[i, j] * sin(-theta)) / mu
            a2 = x[i, j] * sin(-theta) + y[i, j] * cos(-theta)
            x[i, j] = a1 * cos(theta) - a2 * sin(theta)
            y[i, j] = a1 * sin(theta) + a2 * cos(theta)
            m += sqrt(x[i, j] ** 2 + y[i, j] ** 2) * img[j, i]

    m /= (10 * Txy)
    x /= m
    y /= m
    x = x.astype(int)
    y = y.astype(int)
    ic = ic.astype(int)
    jc = jc.astype(int)
    x += ic
    y += jc

    for i in range(i_n):
        for j in range(j_n):
            res[y[i, j], x[i, j]] = img[j, i]
    return res

def main_func():
    files = []
    afiles = []
    dist = []
    for i in range(len(filenames)):
        main = imread(filenames[i])
        main = affine(main)
        files.append(np.fft.fft(main))
        afiles.append(np.fft.fft(main))
        imsave(arr=main, fname='res_' + str(i + 1) + '.png', cmap=cm.gray)

    for i in range(len(afiles)):
        afiles[i] = abs(afiles[i])

    for i in range(len(afiles)):
        dist.append(cdist(afiles[i], afiles[0], metric='seuclidean', p=2))
        d = np.linalg.norm(afiles[i] - afiles[0], ord=2)
        print(d)

main_func()
