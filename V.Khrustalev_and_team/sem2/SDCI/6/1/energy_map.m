
%%% energy_map


function [map] = energy_map(filt_im)
mapz={};
mapz{end+1} = my_mean(filt_im{1}, filt_im{5});
mapz{end+1} = my_mean(filt_im{2}, filt_im{6});
mapz{end+1} = filt_im{11};
mapz{end+1} = my_mean(filt_im{9}, filt_im{13});
mapz{end+1} = my_mean(filt_im{3}, filt_im{7});
mapz{end+1} = filt_im{12}; 
mapz{end+1} = my_mean(filt_im{14}, filt_im{10});   
mapz{end+1} = my_mean(filt_im{4}, filt_im{8});
mapz{end+1} = filt_im{15};
map = mapz;
end

function [ret] = my_mean(A, B)
    ret = A;
    for i = 1:size(A,1)
      for j = 1:size(A,2)
        ret(i,j) = (A(i, j) + B(i, j)) / 2;
      endfor
    endfor
endfunction