%%%%%% filtering.m
function [filt] = filtering(img, mask)
F={};
F{end+1} = imfilter(img, mask((5*1-4):(5*1), 1:5));
F{end+1} = imfilter(img, mask((5*2-4):(5*2), 1:5));
F{end+1} = imfilter(img, mask((5*3-4):(5*3), 1:5));
F{end+1} = imfilter(img, mask((5*4-4):(5*4), 1:5));
F{end+1} = imfilter(img, mask((5*5-4):(5*5), 1:5));

F{end+1} = imfilter(img, mask((5*6-4):(5*6), 1:5));
F{end+1} = imfilter(img, mask((5*7-4):(5*7), 1:5));
F{end+1} = imfilter(img, mask((5*8-4):(5*8), 1:5));
F{end+1} = imfilter(img, mask((5*9-4):(5*9), 1:5));
F{end+1} = imfilter(img, mask((5*10-4):(5*10), 1:5));

F{end+1} = imfilter(img, mask((5*11-4):(5*11), 1:5));
F{end+1} = imfilter(img, mask((5*12-4):(5*12), 1:5));
F{end+1} = imfilter(img, mask((5*13-4):(5*13), 1:5));
F{end+1} = imfilter(img, mask((5*14-4):(5*14), 1:5));
F{end+1} = imfilter(img, mask((5*15-4):(5*15), 1:5));
F{end+1} = imfilter(img, mask((5*16-4):(5*16), 1:5));

filt = F;
end