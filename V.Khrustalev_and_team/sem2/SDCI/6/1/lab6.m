
%%%%%%%% lab6.m
for i = 1:4
    switch i
        case 1
            img_1 = imread('D71.gif');
        case 2
            img_2 = imread('D88.gif');
        case 3
            img_3 = imread('D95.gif');
        case 4
            img_4 = imread('D99.gif');
    end
end

im = [img_1 img_2; img_3 img_4];
figure, imshow(im);
title('Mosaic of images 1 & 2 & 3 & 4');

N = 15;

%создание масок
mask = masks();

%фильтрация изображений
mas_filt_1 = filtering(img_1, mask);
mas_filt_2 = filtering(img_2, mask);
mas_filt_3 = filtering(img_3, mask);
mas_filt_4 = filtering(img_4, mask);

%формирование энергетических текстурных карт и
%формирование вектора текстурных признаков
mas_en_1 = energy_map(mas_filt_1);
mas_en_2 = energy_map(mas_filt_2);
mas_en_3 = energy_map(mas_filt_3);
mas_en_4 = energy_map(mas_filt_4);

p1 = {};
for i = 1:9
    temp = [mas_en_1{i}; mas_en_2{i}];
    p1{end+1} = temp;
end
p2 = {};
for i = 1:9
    temp = [mas_en_3{i}; mas_en_4{i}];
    p2{end+1} = temp;
end

%вычисление м. ожиданий и ср.кв. распределений
for i = 1:9
    m_1{i} = mean(mas_en_1{i});
    m_2{i} = mean(mas_en_2{i});
    m_3{i} = mean(mas_en_3{i});
    m_4{i} = mean(mas_en_4{i});
    
    sd_1{i} = var(double(mas_en_1{i}));
    sd_2{i} = var(double(mas_en_2{i}));
    sd_3{i} = var(double(mas_en_3{i}));
    sd_4{i} = var(double(mas_en_4{i}));
end

%вычисление расстояния Фишера
for i = 1:9
    d_12{i} = (m_1{i} - m_2{i}) / (sd_1{i} - sd_2{i});
    %d_13{i} = (m_1{i} - m_3{i}) / (sd_1{i} - sd_3{i});
    %d_14{i} = (m_1{i} - m_4{i}) / (sd_1{i} - sd_4{i});
    %d_23{i} = (m_2{i} - m_3{i}) / (sd_2{i} - sd_3{i});
    %d_24{i} = (m_2{i} - m_4{i}) / (sd_2{i} - sd_4{i});
    d_34{i} = (m_3{i} - m_4{i}) / (sd_3{i} - sd_4{i});
end

%k-признаки
r_1 = {};
r_2 = {};
r_3 = {};
r_4 = {};
LR1 = {};
LR2 = {};
numb_func = 4
numb_func_ctr = 0
for i = 1:9
    if(d_12{i} > 0.022)
        r_1{end+1} = m_1{i};
        r_2{end+1} = m_2{i};
        LR1{end+1} = p1{i};
		numb_func_ctr = numb_func_ctr + 1
		if (numb_func == numb_func_ctr)
			break;
		end
    end
end

numb_func = 4
numb_func_ctr = 0
for i = 1:9
    if(d_34{i} > 0.019)
        r_3{end+1} = m_3{i};
        r_4{end+1} = m_4{i};
        LR2{end+1} = p2{i};
		numb_func_ctr = numb_func_ctr + 1
		if (numb_func == numb_func_ctr)
			break;
		end
    end
end

%сегментация
im1 = {};
size_win = 50; % 2,5,15,40,75,100
win = ones(size_win, size_win) / (size_win^2);
for i=1:size(LR1, 2)
    im1{end+1}=imfilter(LR1{i}, win, 'conv', 'symmetric');
end

T1 = [size(im1{1}, 1); size(im1, 2)];
P1 = cell(1, size(im1, 2));

for i = 1:size(im1, 2)
       for k = 1:size(im1{1}, 1)
          for j = 1:size(im1{1}, 2)
                 P1{i} = im1{i}(k, :);
                 X1 =norm(double(P1{i}(j)) - double(r_1{i}(j)), 2);
                 X2 = norm(double(P1{i}(j)) - double(r_2{i}(j)), 2);
                if(X1 > X2)
                    T1(k, j) = 0;
                else
                    T1(k, j) = 0.33;
                end
                
          end
       
       end
end

im2 = {};
for i=1:size(LR2, 2)
    im2{end+1}=imfilter(LR2{i}, win, 'conv', 'symmetric');
end

T2 = [size(im2{1}, 1); size(im2, 2)];
P2 = cell(1, size(im2, 2));

for i = 1:size(im2, 2)
       for k = 1:size(im2{1}, 1)
          for j = 1:size(im2{1}, 2)
                 P2{i} = im2{i}(k, :);
                 X1 = norm(double(P2{i}(j)) - double(r_3{i}(j)), 2);
                 X2 = norm(double(P2{i}(j)) -  double(r_4{i}(j)), 2);
                if(X1 > X2)
                    T2(k, j) = 0.66;
                else
                    T2(k, j) = 1;
                end
                
          end
       
       end
end
figure, imshow([T1(1:640, :) T1(641:1280, :); T2(1:640, :) T2(641:1280, :)]);

