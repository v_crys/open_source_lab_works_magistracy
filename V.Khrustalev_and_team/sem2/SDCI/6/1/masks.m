%%%%%% masks.m
function [mask] = masks()
%фильтр для симметричного взвешенного усреднения
L5 = [1 4 6 4 1];
%фильтр для обнаружения краев
E5 = [-1 -2 0 2 1];
%фильтр для обнаружения пятен
S5 = [-1 0 2 0 -1];
%фильтр для обнаружения "ряби"
R5 = [1 -4 6 -4 1];

%L5E5
v = L5' * E5;
mask = v;
%L5R5
v = L5' * R5;
mask = [mask; v];
%E5S5
v = E5' * S5;
mask = [mask; v];
%E5R5
v = E5' * R5;
mask = [mask; v];
%E5L5
v = E5' * L5;
mask = [mask; v];
%R5L5
v = R5' * L5;
mask = [mask; v];
%S5E5
v = S5' * E5;
mask = [mask; v];
%R5E5
v = R5' * E5;
mask = [mask; v];
%L5S5
v = L5' * S5;
mask = [mask; v];
%S5R5
v = S5' * R5;
mask = [mask; v];
%E5E5
v = E5' * E5;
mask = [mask; v];
%S5S5
v = S5' * S5;
mask = [mask; v];
%S5L5
v = S5' * L5;
mask = [mask; v];
%R5S5
v = R5' * S5;
mask = [mask; v];
%R5R5
v = R5' * R5;
mask = [mask; v];
%L5L5
v = L5' * L5;
mask = [mask; v];
end