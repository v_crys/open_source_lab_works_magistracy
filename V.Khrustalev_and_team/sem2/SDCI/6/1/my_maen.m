function [ret] = my_mean(A, B)
    ret = A;
    for i = 1:size(A,1)
      for j = 1:size(A,2)
        ret[i][j] = (A[i][j] + B[i][j]) / 2;
      endfor
    endfor
endfunction
