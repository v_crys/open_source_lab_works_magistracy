%%%%%%%% lab6.m
for i = 1:4
    switch i
        case 1
            img_1 = imread('.\my\D71.jpg');
        case 2
            img_2 = imread('.\my\D88.jpg');
        case 3
            img_3 = imread('.\my\D95.jpg');
        case 4
            img_4 = imread('.\my\D99.jpg');
    end
end

im = [img_1 img_2; img_3 img_4];
figure, imshow(im);
title('Mosaic of images 1 & 2 & 3 & 4');

N = 15;

%создание масок
mask = masks();

%фильтрация изображений
mas_filt_1 = filtering(img_1, mask);
mas_filt_2 = filtering(img_2, mask);
mas_filt_3 = filtering(img_3, mask);
mas_filt_4 = filtering(img_4, mask);

%формирование энергетических текстурных карт и
%формирование вектора текстурных признаков
mas_en_1 = energy_map(mas_filt_1);
mas_en_2 = energy_map(mas_filt_2);
mas_en_3 = energy_map(mas_filt_3);
mas_en_4 = energy_map(mas_filt_4);

p1 = {};
for i = 1:9
    temp = [mas_en_1{i}; mas_en_2{i}];
    p1{end+1} = temp;
end
p2 = {};
for i = 1:9
    temp = [mas_en_3{i}; mas_en_4{i}];
    p2{end+1} = temp;
end

%вычисление м. ожиданий и ср.кв. распределений
for i = 1:9
    m_1{i} = mean(mas_en_1{i});
    m_2{i} = mean(mas_en_2{i});
    m_3{i} = mean(mas_en_3{i});
    m_4{i} = mean(mas_en_4{i});
    
    sd_1{i} = var(double(mas_en_1{i}));
    sd_2{i} = var(double(mas_en_2{i}));
    sd_3{i} = var(double(mas_en_3{i}));
    sd_4{i} = var(double(mas_en_4{i}));
end

%вычисление расстояния Фишера
for i = 1:9
    d_12{i} = (m_1{i} - m_2{i}) / (sd_1{i} - sd_2{i});
    %d_13{i} = (m_1{i} - m_3{i}) / (sd_1{i} - sd_3{i});
    %d_14{i} = (m_1{i} - m_4{i}) / (sd_1{i} - sd_4{i});
    %d_23{i} = (m_2{i} - m_3{i}) / (sd_2{i} - sd_3{i});
    %d_24{i} = (m_2{i} - m_4{i}) / (sd_2{i} - sd_4{i});
    d_34{i} = (m_3{i} - m_4{i}) / (sd_3{i} - sd_4{i});
end

%k-признаки
r_1 = {};
r_2 = {};
r_3 = {};
r_4 = {};
LR1 = {};
LR2 = {};
for i = 1:9
    if(d_12{i} > 0.022)
        r_1{end+1} = m_1{i};
        r_2{end+1} = m_2{i};
        LR1{end+1} = p1{i};
    end
    if(d_34{i} > 0.019)
        r_3{end+1} = m_3{i};
        r_4{end+1} = m_4{i};
        LR2{end+1} = p2{i};
    end
end

%сегментация
im1 = {};
size_win = 2; % 2,5,15,40,75,100
win = ones(size_win, size_win) / (size_win^2);
for i=1:size(LR1, 2)
    im1{end+1}=imfilter(LR1{i}, win, 'conv', 'symmetric');
end

T1 = [size(im1{1}, 1); size(im1, 2)];
P1 = cell(1, size(im1, 2));

for i = 1:size(im1, 2)
       for k = 1:size(im1{1}, 1)
          for j = 1:size(im1{1}, 2)
                 P1{i} = im1{i}(k, :);
                 X1 = dist(double(P1{i}(j)), double(r_1{i}(j)));
                 X2 = dist(double(P1{i}(j)), double(r_2{i}(j)));
                if(X1 > X2)
                    T1(k, j) = 0;
                else
                    T1(k, j) = 0.5;
                end
                
          end
       
       end
end

im2 = {};
for i=1:size(LR2, 2)
    im2{end+1}=imfilter(LR2{i}, win, 'conv', 'symmetric');
end

T2 = [size(im2{1}, 1); size(im2, 2)];
P2 = cell(1, size(im2, 2));

for i = 1:size(im2, 2)
       for k = 1:size(im2{1}, 1)
          for j = 1:size(im2{1}, 2)
                 P2{i} = im2{i}(k, :);
                 X1 = dist(double(P2{i}(j)), double(r_3{i}(j)));
                 X2 = dist(double(P2{i}(j)), double(r_4{i}(j)));
                if(X1 > X2)
                    T2(k, j) = 0.75;
                else
                    T2(k, j) = 1;
                end
                
          end
       
       end
end
figure, imshow([T1(1:640, :) T1(641:1280, :); T2(1:640, :) T2(641:1280, :)]);

%%%%%% masks.m
function [mask] = masks()
%фильтр для симметричного взвешенного усреднения
L5 = [1 4 6 4 1];
%фильтр для обнаружения краев
E5 = [-1 -2 0 2 1];
%фильтр для обнаружения пятен
S5 = [-1 0 2 0 -1];
%фильтр для обнаружения "ряби"
R5 = [1 -4 6 -4 1];

%L5E5
v = L5' * E5;
mask = v;
%L5R5
v = L5' * R5;
mask = [mask; v];
%E5S5
v = E5' * S5;
mask = [mask; v];
%E5R5
v = E5' * R5;
mask = [mask; v];
%E5L5
v = E5' * L5;
mask = [mask; v];
%R5L5
v = R5' * L5;
mask = [mask; v];
%S5E5
v = S5' * E5;
mask = [mask; v];
%R5E5
v = R5' * E5;
mask = [mask; v];
%L5S5
v = L5' * S5;
mask = [mask; v];
%S5R5
v = S5' * R5;
mask = [mask; v];
%E5E5
v = E5' * E5;
mask = [mask; v];
%S5S5
v = S5' * S5;
mask = [mask; v];
%S5L5
v = S5' * L5;
mask = [mask; v];
%R5S5
v = R5' * S5;
mask = [mask; v];
%R5R5
v = R5' * R5;
mask = [mask; v];
%L5L5
v = L5' * L5;
mask = [mask; v];
end

%%%%%% filtering.m
function [filt] = filtering(img, mask)
F={};
F{end+1} = imfilter(img, mask((5*1-4):(5*1), 1:5));
F{end+1} = imfilter(img, mask((5*2-4):(5*2), 1:5));
F{end+1} = imfilter(img, mask((5*3-4):(5*3), 1:5));
F{end+1} = imfilter(img, mask((5*4-4):(5*4), 1:5));
F{end+1} = imfilter(img, mask((5*5-4):(5*5), 1:5));

F{end+1} = imfilter(img, mask((5*6-4):(5*6), 1:5));
F{end+1} = imfilter(img, mask((5*7-4):(5*7), 1:5));
F{end+1} = imfilter(img, mask((5*8-4):(5*8), 1:5));
F{end+1} = imfilter(img, mask((5*9-4):(5*9), 1:5));
F{end+1} = imfilter(img, mask((5*10-4):(5*10), 1:5));

F{end+1} = imfilter(img, mask((5*11-4):(5*11), 1:5));
F{end+1} = imfilter(img, mask((5*12-4):(5*12), 1:5));
F{end+1} = imfilter(img, mask((5*13-4):(5*13), 1:5));
F{end+1} = imfilter(img, mask((5*14-4):(5*14), 1:5));
F{end+1} = imfilter(img, mask((5*15-4):(5*15), 1:5));
F{end+1} = imfilter(img, mask((5*16-4):(5*16), 1:5));

filt = F;
end

%%% energy_map
function [map] = energy_map(filt_im)
mapz={};
mapz{end+1} = wfusmat(filt_im{1}, filt_im{5},'mean');
mapz{end+1} = wfusmat(filt_im{2}, filt_im{6},'mean');
mapz{end+1} = filt_im{11};
mapz{end+1} = wfusmat(filt_im{9}, filt_im{13},'mean');
mapz{end+1} = wfusmat(filt_im{3}, filt_im{7},'mean');
mapz{end+1} = filt_im{12}; 
mapz{end+1} = wfusmat(filt_im{14}, filt_im{10},'mean');   
mapz{end+1} = wfusmat(filt_im{4}, filt_im{8},'mean');
mapz{end+1} = filt_im{15};
map = mapz;
end