import numpy
import numpy as np
from math import sqrt, atan2, cos, sin
from matplotlib.pyplot import imread, imsave, cm
from scipy.spatial.distance import cdist
import cv2
import scipy
import scipy.ndimage
from scipy.spatial import distance


def gen_mask(numb_vect1, numb_vect2):
    ## фильтр для симметричного взвешенного усреднения
    L5 = np.array([1, 4, 6, 4, 1])
    ## фильтр для обнаружения краев
    E5 = np.array([-1, -2, 0, 2, 1])
    ## фильтр для обнаружения пятен
    S5 = np.array([-1, 0, 2, 0, -1])
    ## фильтр для обнаружения "ряби"
    R5 = np.array([1, -4, 6, -4, 1])

    arr_filters = np.array([L5, E5, S5, R5])

    vector_tr = arr_filters[numb_vect1].reshape(5, 1)
    vector2 = arr_filters[numb_vect2].reshape(1, 5)
    return np.dot(vector_tr, vector2)

# функция принимает два масисва, на выходе массив, полученный поэлементным усреднением элементов двух массивов
def mean_2d_arrs(arr1, arr2):
    out_arr = arr1
    for x in range(arr1.shape[1]):
        for y in range(arr1.shape[0]):
            out_arr[x][y] = (arr1[x][y] + arr2[x][y]) / 2
    return out_arr


filenames = ['../D71.jpg', '../D88.jpg', '../D95.jpg', '../D99.jpg']


def main():
    arr_img = []

    # загрузка изображений
    for i in range(len(filenames)):
        arr_img.append(cv2.imread(filenames[i], cv2.IMREAD_GRAYSCALE))

    # оздание мозаики из изображений
    im_v1 = cv2.vconcat([arr_img[0], arr_img[1]])
    im_v2 = cv2.vconcat([arr_img[2], arr_img[3]])
    mosaic_img = cv2.hconcat([im_v1, im_v2])
    # сохранение созданной мозаики
    cv2.imwrite("mosaic.jpg", mosaic_img)

    # все действия будут производится в том числе и с мозаикой текстур, на которой мы должны определить
    # класс текстур
    arr_img.append(mosaic_img)

    # Создание двумерных масок
    mask_arr = []
    for i in range(4):
        for j in range(4):
            mask_arr.append(gen_mask(i, j))

    # устранение влияния интенсивности освещенности
    osv_img_arr = []
    for i in range(5):  # для каждого изображения включая мозаику
        osv_img_arr.append(arr_img[i])
        for x in range(arr_img[i].shape[1]):
            for y in range(arr_img[i].shape[0]):  # проходим по всем пикселям
                st_x = x - 7
                st_y = y - 7
                end_x = x + 7
                end_y = y + 7
                if st_x < 0: st_x = 0
                if st_y < 0: st_y = 0
                if end_x > arr_img[i].shape[1] - 1: end_x = arr_img[i].shape[1] - 1
                if end_y > arr_img[i].shape[0] - 1: end_y = arr_img[i].shape[0] - 1
                # рассчет среднего значения пикселей в окне
                mean = 0
                for x_l in range(st_x, end_x):
                    for y_l in range(st_y, end_y):
                        mean += arr_img[i][x_l][y_l]
                mean /= (end_x - st_x) * (end_y - st_y)
                # компенсация
                osv_img_arr[i][x][y] -= mean
        cv2.imwrite("osv_img_" + str(i) + ".jpg", osv_img_arr[i])

    # фильтрация изображений (свертка изображений с этапа "устранение влияния интенсивности освещенности" с
    # каждой из 16 масок Лавса
    filtering_img_arr = []
    for i in range(5):  # для каждого изображения включая мозаику
        tmp_arr = []
        for j in range(16):
            tmp_arr.append(scipy.ndimage.convolve(osv_img_arr[i], mask_arr[j], mode='nearest'))
        filtering_img_arr.append(tmp_arr)

    # формироване 16 энергетических текстурных карт
    # на основе фильтрации изображений масками Лавса происходит формирование 16 энергетических текстурных карт,
    # путем усреднения в сканирующем окне 15*15
    en_map_arr = []
    for i in range(5): # для каждого изображения включая мозаику
        tmp_arr_for_mask = []
        for z in range(16): # для каждой из 16 масок
            tmp_arr_for_mask.append(filtering_img_arr[i][z])
            for x in range(filtering_img_arr[i][z].shape[1]):
                for y in range(filtering_img_arr[i][z].shape[0]):  # проходим по всем пикселям
                    # устанавливаем границы для окна
                    st_x = x - 7
                    st_y = y - 7
                    end_x = x + 7
                    end_y = y + 7
                    if st_x < 0: st_x = 0
                    if st_y < 0: st_y = 0
                    if end_x > tmp_arr_for_mask[z].shape[1] - 1: end_x = tmp_arr_for_mask[z].shape[1] - 1
                    if end_y > tmp_arr_for_mask[z].shape[0] - 1: end_y = tmp_arr_for_mask[z].shape[0] - 1
                    # рассчет среднего значения в окне
                    mean = 0
                    for x_l in range(st_x, end_x):
                        for y_l in range(st_y, end_y):
                            mean += filtering_img_arr[i][z][x_l][y_l]
                    mean /= (end_x - st_x) * (end_y - st_y)
                    # формирование
                    tmp_arr_for_mask[z][x][y] = mean
        en_map_arr.append(tmp_arr_for_mask)

    # формирование вектора текстурных признаков
    # таблица соответствий между векторами Лавса и номерами 16 карт
    # 0     0   |   L5L5    |   0
    # 0     1   |   L5E5    |   1
    # 0     2   |   L5S5    |   2
    # 0     3   |   L5R5    |   3
    # 1     0   |   E5L5    |   4
    # 1     1   |   E5E5    |   5
    # 1     2   |   E5S5    |   6
    # 1     3   |   E5R5    |   7
    # 2     0   |   S5L5    |   8
    # 2     1   |   S5E5    |   9
    # 2     2   |   S5S5    |   10
    # 2     3   |   S5R5    |   11
    # 3     0   |   R5L5    |   12
    # 3     1   |   R5E5    |   13
    # 3     2   |   R5S5    |   14
    # 3     3   |   R5R5    |   15
    final_maps = []
    for i in range(5): # для каждого изображения включая мозаику
        loc_final_maps = []
        # добавляем (L5E5, E5L5)
        loc_final_maps.append(mean_2d_arrs(en_map_arr[i][1], en_map_arr[i][4]))
        # добавляем (L5R5, R5L5)
        loc_final_maps.append(mean_2d_arrs(en_map_arr[i][3], en_map_arr[i][12]))
        # добавляем E5E5
        loc_final_maps.append(en_map_arr[i][5])
        # добавляем (L5S5, S5L5)
        loc_final_maps.append(mean_2d_arrs(en_map_arr[i][2], en_map_arr[i][8]))
        # добавляем (E5S5, S5E5)
        loc_final_maps.append(mean_2d_arrs(en_map_arr[i][6], en_map_arr[i][9]))
        # добавляем S5S5
        loc_final_maps.append(en_map_arr[i][10])
        # добавляем (R5S5, S5R5)
        loc_final_maps.append(mean_2d_arrs(en_map_arr[i][14], en_map_arr[i][11]))
        # добавляем (E5R5, R5E5)
        loc_final_maps.append(mean_2d_arrs(en_map_arr[i][7], en_map_arr[i][13]))
        # добавляем R5R5
        loc_final_maps.append(en_map_arr[i][15])
        final_maps.append(loc_final_maps)

    # сегментация изображения
    ## рассчет мат. ожидания и ср. квадр. откл
    mu = []
    deviation = []
    for i in range(4): # для каждого изображения
        mu_loc = []
        deviation_loc = []
        for j in range(9): # для каждого из 9 признаков
            mu_loc.append(numpy.mean(final_maps))
            deviation_loc.append(numpy.std(final_maps[i][j]))
        mu.append(mu_loc)
        deviation.append(deviation_loc)

    ## Вычисление расстояния Фишера
    fisher_dist = []
    for i in range(4): # перебираем все пары присутствующих в изображении текстур
        for j in range(4):
            if (i < j): continue
            if (i == j): continue
            # вычисляем растояние Фишера между значениями каждого признака в каждой паре текстур
            loc_dist = []
            for z in range(9):
                loc_dist.append((mu[i][z] - mu[j][z]) / (deviation[i][z] + deviation[j][z]))

            fisher_dist.append(loc_dist)

    ## ИСпользуем расстояне Фишера для выбора наиболее эффективных признаков.
    ## ВЫбираются K признаков, для которых расстояние для какой0либо пары классов текстур наибольшее.
    ## Чем больше расстояние, тем больше вероятность правильного определения текстуры так как признаки будут
    ## наиболее показательными.
    K = 4 ## будем выбирать K признаков
    ## считаем веса каждого из признаков (чем больше вес, тем лучше признак в нашем случае)
    weight = np.zeros(9)
    for i in range(len(fisher_dist)): # перебираем все пары
        for j in range(9): # перебираем все 9 признаков
            weight[j] += fisher_dist[i][j]

    ## находи K признаокв с наибольшим весом
    arr_best_signs = []
    for z in range(K):
        max_val = 0
        numb_value = 0
        for i in range(9):
            if weight[i] > max_val:
                max_val = weight[i]
                numb_value = i
        arr_best_signs.append(numb_value)
        weight[numb_value] = 0

    ## формируем эталонные вектора признаокв из мат ожиданий
    R_arr = []
    for i in range(4): # для каждого изображения
        r_loc = []
        for j in range(K): # из K лучших признаков
            r_loc.append(mu[i][arr_best_signs[j]])
        R_arr.append(r_loc)

    # СЕГМЕНТАЦИЯ
    # Будет построена карта сегментации (изображение) на котором пиксели будут иметь
    # 4 уровня яркости (каждый уровень яркости соответствует своей текстуре)
    # На ранних этапах были посчитаны текстурные признаки для мозаики, необходимо оставить только наиболее эффективные

    mosaic_LF = []
    for j in range(K):  # из K лучших признаков
        mosaic_LF.append(final_maps[4][arr_best_signs[j]])

    segmentation_map = mosaic_img

    # Проходим по всем пикселям изображения и усредняем в окне
    window_size = 2
    for x in range(segmentation_map.shape(1)):
        for y in range(segmentation_map.shape(0)):
            for z in range(K): # лучшие признаки
                st_x = x - (window_size/2)
                st_y = y - (window_size/2)
                end_x = x + (window_size/2)
                end_y = y + (window_size/2)
                if st_x < 0: st_x = 0
                if st_y < 0: st_y = 0
                if end_x > mosaic_LF[z].shape[1] - 1: end_x = mosaic_LF[z].shape[1] - 1
                if end_y > mosaic_LF[z].shape[0] - 1: end_y = mosaic_LF[z].shape[0] - 1
                # рассчет среднего значения в окне
                mean = 0
                for x_l in range(st_x, end_x):
                    for y_l in range(st_y, end_y):
                        mean += mosaic_LF[z][x_l][y_l]
                mean /= (end_x - st_x) * (end_y - st_y)
                # усреднение
                mosaic_LF[z][x][y] = mean

    # для каждого пикселя определяем его принадлежность к какой либо текстуре
    for x in range(segmentation_map.shape(1)):
        for y in range(segmentation_map.shape(0)):
            # считаем растояние между векторами текстур и мозаики
            pix_vector = [mosaic_LF[0][x][y], mosaic_LF[1][x][y], mosaic_LF[2][x][y], mosaic_LF[3][x][y]]
            arr_dist = []
            for i in range(4): # перебираем все текстуры
                arr_dist.append(distance.euclidean(R_arr[i], pix_vector))
            # находим наименьшее растояние
            min_dist = arr_dist[0]
            min_dist_numb = 0
            for i in range(4):
                if (arr_dist[i] < min_dist):
                    min_dist = arr_dist[i]
                    min_dist_numb = i

            # красим пиксель в цвет соответствующей текстуре
            segmentation_map[x][y] = min_dist_numb * (255 / 3)

    # сохраняем полученное сигментированное изображение
    cv2.imwrite("result.jpg", segmentation_map)


main()
