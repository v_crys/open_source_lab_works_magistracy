import numpy as np
import math
import cv2
from scipy.spatial import distance

# -----------------------------------------------------------
# Глобальные настройки
# -----------------------------------------------------------

# пути к исходным изображениям
filenames = ['../D6.jpg', '../D8.jpg', '../D77.jpg', '../D20.jpg']

# количество уровней яркости в изображении (производится квантование
# исходного изображения до заданного
# количества уровней)
L = 8

# 8 признаков
numb_of_signs = 8

# -----------------------------------------------------------
# Функции для построения матрицы рассеяния
# -----------------------------------------------------------

# функция для построения матрицы рассеяния
# img - изображение (двумерный массив)
# d_r - вектор d (счещение по x)
# d_c - вектор d (счещение по y)
def calc_Nd_matrix(img, d_r, d_c):
    Nd = np.zeros((L, L))
    # Проходим по всем пикселям изображения и подсчиытвем количество
    # переходов из одного значения яркости пикселя, в другое.
    # Полученная матрица будет квадратной размером 256*256 (если каждый
    # пиксель кодируется 256 значениями яркости)
    # Значение в полученной матрице [1][0], например, будет
    # соотвествовать КОЛИЧЕТСВУ переходов яркости пикселя от 1 к 0.
    # Верктор {d_r, d_c} задает переход от одного пикселя к другому.
    for x in range(img.shape[1]):
        for y in range(img.shape[0]):
            a = img[y][x]  # значение яркости одного пикселя
            # защита от перехода через границу изображения
            d_x = int(x + d_r)
            d_y = int(y + d_c)
            if (d_x < 0) or (d_x >= img.shape[1]): continue
            if (d_y < 0) or (d_y >= img.shape[0]): continue
            b = img[d_y][d_x]  # значение яркости второго пикселя со смещением d
            # так как изобраение было квантовано, учитываем это
            a //= (256 / L)
            b //= (256 / L)
            # увеличиваем соответствующий счетчик
            Nd[int(a)][int(b)] += 1

    return Nd


# функция для нормировки матрицы рассеяния (нормированная - это
# значит что сумма всех элементов равна 1)
#
# Полученная матрица может быть рассмотрена как гистограмма второго порядка
# Nd - матрица рассеяния
def calc_p_matrix(Nd):
    p = np.zeros((L, L))
    # находим сумму элементов в матрице рассеяния
    Nd_sum = 0
    for a in range(L):
        for b in range(L):
            Nd_sum += Nd[a][b]
    # нормируем
    for a in range(L):
        for b in range(L):
            p[a][b] = Nd[a][b] / Nd_sum

    return p


# -----------------------------------------------------------
# функции для рассчета статистических моментов первого и
# второго порядков
#
# Таких моментов два: мат.ожидание и ср.квадр.откл.
# Для каждого компоеннта a и b своя
# -----------------------------------------------------------

# мат. ожидание для компоенента a
# p - нормализованеная матрица рассеяния
def moment_mean_a(p):
    ret = 0
    for a in range(L):
        for b in range(L):
            # a+1 так как нету смысла умножать на ноль
            # в формуле методички очевидно ошика
            ret += (a + 1) * p[a][b]
    return ret


# мат. ожидание для компоенента b
# p - нормализованеная матрица рассеяния
def moment_mean_b(p):
    ret = 0
    for a in range(L):
        for b in range(L):
            # b+1 так как нету смысла умножать на ноль
            ret += (b + 1) * p[a][b]
    return ret


# средне квадратическое отклонение для компоенента a
# p - нормализованеная матрица рассеяния
# a_m - мат. ожидание для компоенента a (см. moment_mean_a)
def moment_deviation_a(p, a_m):
    ret = 0
    for a in range(L):
        for b in range(L):
            # a+1 так как нету смысла умножать на ноль
            ret += ((a + 1) ** 2) * p[a][b]
    # Внимательно посмотрите на формулу!
    return math.sqrt(ret - (a_m ** 2))


# средне квадратическое отклонение для компоенента b
# p - нормализованеная матрица рассеяния
# b_m - мат. ожидание для компоенента b (см. moment_mean_b)
def moment_deviation_b(p, b_m):
    ret = 0
    for a in range(L):
        for b in range(L):
            # b+1 так как нету смысла умножать на ноль
            ret += ((b + 1) ** 2) * p[a][b]
    # Внимательно посмотрите на формулу!
    return math.sqrt(ret - (b_m ** 2))


# -----------------------------------------------------------
# Характеристики, используемые в качестве признаокв текстуры
# По нормированной матрице рассеяния рассчитыается набор
# статистичкеских параемтров, которые можно использовать
# как компоеннты вектора признаокв, описывающего текстуры
# -----------------------------------------------------------

# Рассчет ковариации
# p - нормированная матрица рассеяния
# a_m - мат ожидание по компоеннту a (см. moment_mean_a)
# b_m - мат ожидание по компоеннту b (см. moment_mean_b)
def calc_covariation(p, a_m, b_m):
    ret = 0
    for a in range(L):
        for b in range(L):
            # a+1 b+1 так как нету смысла умножать на ноль
            ret += ((a + 1) - a_m) * ((b + 1) - b_m) * p[a][b]

    return ret


# Рассчет контраста
# p - нормированная матрица рассеяния
def calc_contrast(p):
    ret = 0
    for a in range(L):
        for b in range(L):
            # a+1 b+1 так как нету смысла умножать на ноль
            ret += ((a - b) ** 2) * p[a][b]

    return ret


# Рассчет средней абсолютной разности
# p - нормированная матрица рассеяния
def calc_mean_abs_diff(p):
    ret = 0
    for a in range(L):
        for b in range(L):
            ret += abs(a - b) * p[a][b]

    return ret


# Рассчет энергии
# p - нормированная матрица рассеяния
def calc_energy(p):
    ret = 0
    for a in range(L):
        for b in range(L):
            ret += p[a][b] ** 2

    return ret


# Рассчет энтропии
# p - нормированная матрица рассеяния
def calc_entropy(p):
    ret = 0
    for a in range(L):
        for b in range(L):
            # ограничение python на логарифмы от очень малых чисел
            if p[a][b] < (10 ** -3): continue
            ret += p[a][b] * math.log(2, p[a][b])

    return -1 * ret


# Рассчет обратной разности
# p - нормированная матрица рассеяния
def calc_inverse_diff(p):
    ret = 0
    for a in range(L):
        for b in range(L):
            ret += p[a][b] / (1 + ((a - b) ** 2))

    return ret


# Рассчет однородности
# p - нормированная матрица рассеяния
def calc_homogeneity(p):
    ret = 0
    for a in range(L):
        for b in range(L):
            ret += p[a][b] / (1 + abs(a - b))

    return ret


# Рассчет корреляции
# p - нормированная матрица рассеяния
# a_m - мат ожидание по компоеннту a (см. moment_mean_a)
# b_m - мат ожидание по компоеннту b (см. moment_mean_b)
# a_d - средне квадр откл (см. moment_deviation_a)
# b_d - средне квадр откл (см. moment_deviation_b)
def calc_corelation(p, a_m, b_m, a_d, b_d):
    ret = 0
    for a in range(L):
        for b in range(L):
            ret += ((a + 1) * (b + 1) * p[a][b]) - (a_m * b_m)

    return ret / (a_d * b_d)


# -----------------------------------------------------------
# Вспомогательные функции для алгоритма
# -----------------------------------------------------------

# функция рассчитвает вектор признаков для изображения
# img - входное изображение
# d_r - компонента x вектора смещения
# d_c - компонента y вектора смещения
def calc_vector_sign(img, d_r, d_c):
    # рассчитваем нормированную матрицу рассеяния
    tmp_Nd = calc_Nd_matrix(img, d_r, d_c)
    p_tmp = calc_p_matrix(tmp_Nd)

    # считаем статистические моменты
    a_mean = moment_mean_a(p_tmp)  # мат. ожидания
    b_mean = moment_mean_b(p_tmp)

    a_dev = moment_deviation_a(p_tmp, a_mean)  # ср. квад. отклонения
    b_dev = moment_deviation_b(p_tmp, b_mean)

    # считаем признаки
    vector = np.zeros(numb_of_signs)
    vector[0] = calc_covariation(p_tmp, a_mean, b_mean)
    vector[1] = 0#calc_contrast(p_tmp)
    vector[2] = calc_mean_abs_diff(p_tmp)
    vector[3] = calc_energy(p_tmp)
    vector[4] = 0##calc_entropy(p_tmp)
    vector[5] = 0#calc_inverse_diff(p_tmp)
    vector[6] = 0#calc_homogeneity(p_tmp)
    vector[7] = calc_corelation(p_tmp, a_mean, b_mean, a_dev, b_dev)

    return vector

def main():
    arr_img = []

    # загрузка изображений
    for i in range(len(filenames)):
        arr_img.append(cv2.imread(filenames[i], cv2.IMREAD_GRAYSCALE))

    # оздание мозаики из изображений
    im_v1 = cv2.vconcat([arr_img[0], arr_img[1]])
    im_v2 = cv2.vconcat([arr_img[2], arr_img[3]])
    mosaic_img = cv2.hconcat([im_v1, im_v2])
    # сохранение созданной мозаики
    cv2.imwrite("mosaic.jpg", mosaic_img)

    # квантование изображение мозаики (производится исключительно
    # для увеличения скорости работы программы и уменьшения
    # количества требуемой памяти)
    for x in range(mosaic_img.shape[1]):
        for y in range(mosaic_img.shape[0]):
            # 256 стандартное количесво оттенков серого
            mosaic_img[y][x] //= (256 / L);
            mosaic_img[y][x] *= (256 / L);

    # квантование эталонные избражения (производится исключительно
    # для увеличения скорости работы программы и уменьшения
    # количества требуемой памяти)
    for i in range(len(arr_img)):
        for x in range(arr_img[i].shape[1]):
            for y in range(arr_img[i].shape[0]):
                # 256 стандартное количесво оттенков серого
                arr_img[i][y][x] //= (256 / L);
                arr_img[i][y][x] *= (256 / L);

    # сохраняем квантованное изображение
    cv2.imwrite("mosaic_quant.jpg", mosaic_img)

    # -----------------------------------------------------------
    # Алгоритм. Шаг 1-2. Определение оптимального вектора смещения
    # И построение матрицы рассеяния
    # -----------------------------------------------------------

    # строим набор векторов смещения (8 направлений, 5 длин)
    numb_len_for_vector = 5
    numb_vectors = 8 * numb_len_for_vector
    vectors_d_arr = np.zeros((2, numb_vectors))  # нулевая компонента - x; первая - y
    vectors_d_arr_i = 0
    for len_vect in range(1, numb_len_for_vector+1):  # перебираем все длины
        for x in [-1, 0, 1]:  # все возможные направления по x
            for y in [-1, 0, 1]:  # все возможные направления по y
                if (x == 0) and (y == 0): continue # нулевой вектор нас не интересует
                vectors_d_arr[0][vectors_d_arr_i] = int(x * len_vect)
                vectors_d_arr[1][vectors_d_arr_i] = int(y * len_vect)
                vectors_d_arr_i += 1

    # перебираем все вектора и ищем оптимальный
    ksi_max = 0
    for n_vect in range(numb_vectors):
        # для каждого вектора рассчитваем критерий хи-квадрат
        # для этого необходимо рассчитать матрицу рассеяния и нормирвоать ее
        # для этого используются функции calc_Nd_matrix, calc_p_matrix
        tmp_Nd = calc_Nd_matrix(mosaic_img, vectors_d_arr[0][n_vect], vectors_d_arr[1][n_vect])
        p_tmp = calc_p_matrix(tmp_Nd)

        # рассчитваем хи-квадрат.
        ksi_kv = 0
        for a in range(L):
            for b in range(L):
                # рассчитваем p_a и p_b путем маргинализации матрицы рассеивания
                p_a = 0
                for b_marg in range(L):
                    p_a += p_tmp[a][b_marg]

                p_b = 0
                for a_marg in range(L):
                    p_b += p_tmp[a_marg][b]

                # собственно считаем хи-вадрат
                ksi_kv += (p_tmp[a][b] ** 2) / (p_a * p_b)
        ksi_kv -= 1

        # запоминаем результат, если он лучше предыдущих
        if ksi_max < ksi_kv:
            ksi_max = ksi_kv
            p_matrix = p_tmp.copy()
            d_r = vectors_d_arr[0][n_vect]
            d_c = vectors_d_arr[1][n_vect]

    # -----------------------------------------------------------
    # Алгоритм. Шаг 3. Построение эталонных векторов признаков
    # для набора текстур
    # -----------------------------------------------------------

    # для каждого изображения считаем вектор признаков

    arr_vectors_for_etalon = np.zeros((len(filenames), numb_of_signs))
    for i in range(len(filenames)):
        arr_vectors_for_etalon[i] = calc_vector_sign(arr_img[i], d_r, d_c)

    # -----------------------------------------------------------
    # Алгоритм. Шаг 4. Построение вектора признаков для каждого
    # пикселя подлежащего сегментации изображения
    # -----------------------------------------------------------
    mosaic_vectors_for_pix = np.zeros((mosaic_img.shape[0], mosaic_img.shape[1], numb_of_signs))
    for x in range(mosaic_img.shape[1]):
        for y in range(mosaic_img.shape[0]):
            N = 15  # размер окна
            # границы окна
            x_loc_start = x - int(N / 2)
            y_loc_start = y - int(N / 2)
            x_loc_end = x + int(N / 2)
            y_loc_end = y + int(N / 2)
            if x_loc_start < 0: x_loc_start = 0
            if y_loc_start < 0: y_loc_start = 0
            if x_loc_end >= mosaic_img.shape[1]: x_loc_end = mosaic_img.shape[1] - 1
            if y_loc_end >= mosaic_img.shape[1]: y_loc_end = mosaic_img.shape[1] - 1
            # создаем новое изображение в котором будет находится содержание окна
            window_img = np.zeros((int(x_loc_end - x_loc_start), int(y_loc_end - y_loc_start)))
            for i in range(int(x_loc_end - x_loc_start)):
                for j in range(int(y_loc_end - y_loc_start)):
                    window_img[i][j] = mosaic_img[int(x_loc_start+i)][int(y_loc_start+j)]

            # для изображения в окне рассчитваем вектор признаков по аналогии с пунктом 3
            mosaic_vectors_for_pix[y][x] = calc_vector_sign(window_img, d_r, d_c)

    # -----------------------------------------------------------
    # Алгоритм. Шаг 5. Производим сегментацию.
    # Для каждого пикселя изображения вычисляем Евклидовы
    # расстояния с каждым из эталонных векторов признаков и принимаем
    # решение о принадлежности пикселя к какой либо текстуре
    # -----------------------------------------------------------

    segm_img = mosaic_img.copy()

    # проходим по всем пикселям изображения
    for x in range(mosaic_img.shape[1]):
        for y in range(mosaic_img.shape[0]):
            min_dist = 99999
            number_min_dist = 0
            # находим ближайший вектор признаков
            for i in range(len(arr_vectors_for_etalon)):
                # находим растояние между двумя векторами
                tmp_dist = distance.euclidean(arr_vectors_for_etalon[i], mosaic_vectors_for_pix[y][x])
                if tmp_dist < min_dist:
                    min_dist = tmp_dist
                    number_min_dist = i

            # красим пиксель в цвет соответствующей тексутре
            segm_img[y][x] = (number_min_dist * 255) / (len(filenames) - 1)

    # сохраняем сегментированное изображение
    cv2.imwrite("segmentation_img.jpg", segm_img)

main()
