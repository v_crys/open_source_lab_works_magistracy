for i = 1:2
    %чтение картинок
    if(i == 1)
        I = imread('img1.jpg');
    elseif(i == 2)
        I = imread('img2.jpg');
    end
    figure, imshow(I);
    str = ['Original image #', num2str(i)];
    title(str);
    %бинарная реализация
    %построение гистограммы
    figure;
    [counts, x] = imhist(I, 100);%100 - количество интервалов 
    level = multithresh(I);
    stem(x, counts, '.', 'b');
    str = ['Histogram of image #', num2str(i)];
    title(str);
    hold on 
    plot([level level], [0 max(counts)],'magenta', 'LineWidth', 2);%отметка оптимального порога
    s_I = imquantize(I, level);%квантирует изображение
    figure, imshow(s_I, []);
    str = ['Binary segmentation of image #', num2str(i), ' by Otsu method'];
    title(str);
    %многопороговая реализация
    for j = 2:1:5
        thresh = multithresh(I, j);
        s_I = imquantize(I, thresh);
        rgb = label2rgb(s_I); 
        figure, imshow(rgb);
        str = ['Multithreshold segmentation of image #', num2str(i), ' by Otsu method, M = ', num2str(j+1)];
        title(str);
    end
end 
