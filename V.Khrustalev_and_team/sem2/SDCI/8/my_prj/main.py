import numpy as np
import cv2
from matplotlib import pyplot as plt
from skimage.filters import threshold_multiotsu

img = cv2.imread('../img2.jpg', cv2.IMREAD_GRAYSCALE)   # загружаем изображение в монохромном режиме
for i in range(2, 7): # колличество классов от 2 до 5 (соответственно количество порогов на 1 меньшe)
    # находим пороговый уровень и получем результат по методу Оцу
    line_thresholds = threshold_multiotsu(img, classes=i)
    regions = np.digitize(img, bins=line_thresholds)
    # формируем изображение
    osu_img = regions.copy()
    for x in range(regions.shape[1]):
        for y in range(regions.shape[0]):
            osu_img[y][x] = regions[y][x] * (255 / regions.max()) # нормируем пиксели
    cv2.imwrite("result_" + str(i) + ".jpg", osu_img) # выводим результат алгоритма Отцу
    gist = plt.hist(img.ravel(), 256) # считает гистограмму исходного изображения
    plt.plot(gist[0][0]) # выводим гистограмму
    for j in range(i - 1):  # добавляем на гистограмму оптимальные пороги
        plt.plot((line_thresholds[j], line_thresholds[j]), (0, gist[0][0].max()))
    plt.savefig("img_gist_" + str(i) + ".png")
    plt.clf()