import math
import numpy as np
import numpy as linalg
import random
# ==============================================================
# Лабораторная работа №1 по курсе Интеллектуальные системы
# ==============================================================

# --------------------------------------------------------------
# Структуры
# --------------------------------------------------------------
class Claster(object):
    def __init__(self, center):
        self.center = center
        self.point_arr = []

# --------------------------------------------------------------
# Рассчет статистических характеристик кластера
# --------------------------------------------------------------

# Функция вычисляет вектор средних значений
# M - кластер
# возвращает вектор средних значения
def vector_mean_value(M):
    ret_point = np.zeros(len(M.center))
    for i in range(len(M.center)):
        val = 0
        for j in range(len(M.point_arr)):
            val += M.point_arr[j][i]
        val /= len(M.point_arr)
        ret_point[i] = val

    return ret_point

# Функция вычисляет матрицы ковариации
# M - кластер
# u - вектор средних значений
# возвращает матрицу ковариации
def calc_Cov_matrix(M, u):
    Cov = np.zeros((len(M.center), len(M.center)))

    for i in range(len(M.center)):
        for j in range(i, len(M.center)):
            D_val = 0
            for z in range(len(M.point_arr)):
                D_val += (M.point_arr[z][i] - u[i]) * (M.point_arr[z][j] - u[j])
            D_val /= len(M.point_arr)

            Cov[i][j] = D_val
            Cov[j][i] = D_val

    return Cov


# --------------------------------------------------------------
# Расстояния между образами
# --------------------------------------------------------------

# Рассчем растояния Евклида
# a - первый вектор
# b - второй вектор
# функция возвращает растояние Евклида меэжу векторами a и b
def Distance_F2F_Euclide(a, b):
    dist = 0
    for i in range(len(a)):
        dist += (a[i] - b[i]) ** 2
    return math.sqrt(dist)

# Рассчет Манхеттингского расстояния
# a - первый вектор
# b - второй вектор
# функция возвращает Манхеттенгское растояние меэжу векторами a и b
def Distance_F2F_Manshetinscoe(a, b):
    dist = 0
    for i in range(len(a)):
        dist += abs(a[i] - b[i])
    return dist

# Рассчет расстояние доминирования
# a - первый вектор
# b - второй вектор
# функция возвращает растояние доминирование меэжу векторами a и b
def Distance_F2F_Dominate(a, b):
    dist = 0
    for i in range(len(a)):
        if dist < abs(a[i] - b[i]):
            dist = abs(a[i] - b[i])
    return dist

# Рассчет расстояние Камберра
# a - первый вектор
# b - второй вектор
# функция возвращает растояние Камберра меэжу векторами a и b
def Distance_F2F_Camberra(a, b):
    dist = 0
    for i in range(len(a)):
        dist += abs(a[i] - b[i]) / (abs(a[i]) + abs(b[i]))
    return dist

# Рассчет косинусного расстояния
# a - первый вектор
# b - второй вектор
# функция возвращает косинусное растояние меэжу векторами a и b
def Distance_F2F_Cosinus(a, b):
    dist = 0
    nom = 0
    denom_a = 0
    denom_b = 0
    for i in range(len(a)):
        nom += a[i] * b[i]
        denom_a += a[i] ** 2
        denom_b += b[i] ** 2

    dist = math.acos(nom / (math.sqrt(denom_a) * math.sqrt(denom_b)))
    return dist

# --------------------------------------------------------------
# Расстояния между кластером и образом
# --------------------------------------------------------------

# Рассчет растояния до центра кластера
# C - кластер
# P - образ
# Func - функция для рассчета растояния
def Distance_F2C_Center(C, P, Func):
    m = vector_mean_value(C)
    return Func(m, P)

# Рассчет расстояния до ближайшего образа кластера
# C - кластер
# P - образ
# Func - функция для рассчета растояния
def Distance_F2C_Near(C, P, Func):
    min_dist = 999999
    for i in range(len(C.point_arr)):
        dist = Func(P, C.point_arr[i])
        if min_dist > dist:
            min_dist = dist
    return min_dist

# Евклидово расстояние с учетом дисперсии признаков между точкой
# и кластером в пространстве признаков
# C - кластер
# P - образ
# Func - функция вычисления растояния между двумя точками
# возвращает растояние
def Distance_F2C_Euclide_with_Cov(C, P, Func):
    dist = 0
    # x - вектор признаков точки
    # m - вектор средних значений признаков класса
    # D - дисперсии признаокв (диагональ матрицы ковариации)
    x = P
    m = vector_mean_value(C)

    # если в кластере только одна точка, то ковариационная матрица нулевая
    # А у нулевая, а делить на 0 не хорошо
    # в таком случае просто вычисляем растояние между точкой и точкой
    if len(C.point_arr) == 1:
        return Func(m, x)

    Cov = calc_Cov_matrix(C, m)

    for i in range(len(x)):
        dist += ((x[i] - m[i]) ** 2) / Cov[i][i]
    return math.sqrt(dist)

# Растояние Махаланобиса между точкой
# и кластером в пространстве признаков
# C - кластер
# P - образ
# Func - функция вычисления растояния между двумя точками
# возвращает растояние
def Distance_F2C_Maxalanobis(C, P, Func):
    dist = 0

    # x - вектор признаков точки
    # m - вектор средних значений признаков класса
    # Cov - атрица ковариации

    x = P
    m = vector_mean_value(C)

    # Обратная ковариационная матрица существует если только m>n
    # Или когда количество образов больше, чем количество признаков
    # в случае, если ковариационная матрица не существует, будем вычислять через среднее
    if len(C.point_arr) <= len(C.center):
        return Func(m, x)

    Cov = calc_Cov_matrix(C, m)

    Cov_inv = linalg.inv(Cov)
    for i in range(x.num_axes):
        loc = 0
        for j in range(x.num_axes):
            loc += (x[j] - m[j]) * Cov_inv[i][j]
        dist += loc * (x[i] - m[i])

    return dist


# --------------------------------------------------------------
# Расстояния между кластерами
# --------------------------------------------------------------

# Растояние между центрами кластеров
# C1 - первый кластер
# С2 - второй кластер
# Func - функция для рассчета растояния
# lym - не используется (нужна для совместимости)
def Distance_C2C_Centers(C1, C2, Func, lym):
    m1 = vector_mean_value(C1)
    m2 = vector_mean_value(C2)
    return Func(m1, m2)

# Растояние между ближайшими соседями кластера
# C1 - первый кластер
# С2 - второй кластер
# Func - функция для рассчета растояния
# lym - не используется (нужна для совместимости)
def Distance_C2C_Near(C1, C2, Func, lym):
    min_dist = 999999
    for i in range(len(C1.point_arr)):
        for j in range(len(C2.point_arr)):
            dist = Func(C1.point_arr[i], C2.point_arr[j])
            if dist < min_dist:
                min_dist = dist
    return min_dist


# Растояние между дальними соседями
# C1 - первый кластер
# С2 - второй кластер
# Func - функция для рассчета растояния
# lym - не используется (нужна для совместимости)
def Distance_C2C_Far(C1, C2, Func, lym):
    max_dist = 999999
    for i in range(len(C1.point_arr)):
        for j in range(len(C2.point_arr)):
            dist = Func(C1.point_arr[i], C2.point_arr[j])
            if dist > max_dist:
                max_dist = dist
    return max_dist

# К-растояние
# C1 - первый кластер
# С2 - второй кластер
# Func - функция для рассчета растояния
# lym - целое число в диапазоне +-inf. При lym->inf выражается в растояние дальнего соседа,
# при lym->-inf выражается в растояние ближнего соседа
def Distance_C2C_K(C1, C2, Func, lym):
    dist = 0
    for i in range(len(C1.point_arr)):
        for j in range(len(C2.point_arr)):
            dist += Func(C1.point_arr[i], C2.point_arr[j]) ** lym

    dist /= len(C1.point_arr) * len(C2.point_arr)
    dist **= 1 / lym
    return dist

# --------------------------------------------------------------
# Алгоритмы
# --------------------------------------------------------------

# Пороговый алгоритм кластеризации
# M - входное множество образов (двумерный массив, образ*признак)
# t - пороговое значение расстояния
# Dist_func_F2F - функция для рассчета растояния между образами
# Dist_func_F2С - функция для рассчета растояния между кластером и образом
# функция возвращает массив кластеров
#
# Пороговый алгоритм кластеризации состоит в следующем. Пусть в пространстве признаков задано множество образов
# M={x1, …, xk, …, xm }, где xk – вектор признаков k-го образа, m – мощность множества. Будем считать, что центр
# первого кластера z1 совпадает с любым из образов множества, например с x1, т.е. w1 = x1. Далее вычисляется
# расстояние d21 между образом x2 и центром кластера w1. Напомним, что способ вычисления расстояния между точкой и
# кластером выбирается разработчиком. Если значение расстояния больше заранее заданной пороговой величины t, то
# образ x2 принимается за новый центр кластера w2. В противном случае образ x2 включается в кластер w1.
# Для следующего образа оцени-ваются расстояния от него до имеющихся кластеров. Если все расстояния больше порога,
# то образ принимается за новый кластер. Если часть расстояний меньше порога, то образ относится к ближайшему кластеру.
# Процедура продолжается по-ка не будут исчерпаны все образы множества M.
def alg_Porogovy(M, t, Dist_func_F2F, Dist_func_F2C):
    # инициализируем массив кластеров
    claster_arr = []
    tmp_cl = Claster(M[0])
    tmp_cl.point_arr.append(M[0])
    claster_arr.append(tmp_cl)

    # перебираем все свободные образы
    for i in range(1, len(M)):
        # ищем минимальное растояние от образа до какого либо кластера
        min_dist = 999999
        for j in range(len(claster_arr)):
            dist = Dist_func_F2C(claster_arr[j], M[i], Dist_func_F2F)
            if min_dist > dist:
                min_dist = dist

        if min_dist > t:
            # создаем новый кластер
            tmp_cl = Claster(M[i])
            tmp_cl.point_arr.append(M[i])
            claster_arr.append(tmp_cl)
        else:
            # добавляем к кластеру
            for j in range(len(claster_arr)):
                # проверяем растояние между центром класетар и образом
                dist = Dist_func_F2C(claster_arr[j], M[i], Dist_func_F2F)
                if dist == min_dist:
                    # добавляем в кластер образ
                    claster_arr[j].point_arr.append(M[i])
                    # пересчитываем центр образа
                    claster_arr[j].center = vector_mean_value(claster_arr[j])
                    break

    return claster_arr

# Цепной алгоритм кластеризации
# M - входное множество образов (двумерный массив, образ*признак)
# t - пороговое значение расстояния
# Dist_func - функция для рассчета растояния
# функция возвращает массив кластеров
#
# Метод цепной кластеризации, являющийся разновидностью порогового алгоритма, в котором за расстояние между
# точкой и кластером в пространстве признаков принимается расстояние от точки до ближайшего представителя кластера.
def alg_Cepnoy(M, t, Dist_func):
    # инициализируем массив кластеров
    claster_arr = []
    tmp_cl = Claster(M[0])
    tmp_cl.point_arr.append(M[0])
    claster_arr.append(tmp_cl)

    # перебираем все свободные образы
    for i in range(1, len(M)):
        # щем минимальное растояние от образа до какого либо кластера
        min_dist = 999999
        for j in range(len(claster_arr)):
            dist = Distance_F2C_Near(claster_arr[j], M[i],  Dist_func)
            if min_dist > dist:
                min_dist = dist

        if min_dist > t:
            # создаем новый кластер
            tmp_cl = Claster(M[i])
            tmp_cl.point_arr.append(M[i])
            claster_arr.append(tmp_cl)
        else:
            for j in range(len(claster_arr)):
                # проверяем растояние между центром класетар и образом
                dist = Distance_F2C_Near(claster_arr[j], M[i],  Dist_func)
                if dist == min_dist:
                    # добавляем в кластер образ
                    claster_arr[j].point_arr.append(M[i])
                    # пересчитываем центр образа
                    claster_arr[j].center = vector_mean_value(claster_arr[j])
                    break

    return claster_arr

# Алгоритм кластеризации слиянием
# M - входное множество образов (двумерный массив, образ*признак)
# t - пороговое значение расстояния
# Dist_func_F2F - функция для рассчета растояния между образами
# Dist_func_C2C - функция для рассчета растояния между кластерами
# lym - используется только для К растояния (в остальных алгоритмах не имеет значения)
# функция возвращает массив кластеров
#
# Метод кластеризации слиянием тоже достаточно прост. Вначале каждый образ считается отдельным кластером,
# далее вычисляется расстояние между всеми кластерами, т.е. формируется квадратная, диагонально-симметричная таблица
# расстояний, строки и столбцы которой – имеющиеся кластеры. На каждом шаге сливаются два самых близких кластера,
# после чего размер таблицы уменьшается на единицу и вычисляются новые расстояния между изменившимися кластерами.
# Процесс прекращается при достижении заданного числа кластеров или когда расстояние между ближайшими кластерами
# больше заданной пороговой величины.
def alg_Sliyanie(M, t, Dist_func_F2F, Dist_func_C2C, lym):
    # инициализируем массив кластеров
    claster_arr = []
    for i in range(len(M)):
        tmp_cl = Claster(M[i])
        tmp_cl.point_arr.append(M[i])
        claster_arr.append(tmp_cl)

    while 1:
        # строим квадратную матрицу растояний
        dist_matrix = np.zeros((len(claster_arr), len(claster_arr)))
        for i in range(len(claster_arr)):
            for j in range(len(claster_arr)):
                dist = Dist_func_C2C(claster_arr[i], claster_arr[j], Dist_func_F2F, lym)
                dist_matrix[i][j] = dist
                dist_matrix[j][i] = dist

        # находим два самых близких кластера
        min_dist = 999999
        C1 = 0
        C2 = 0
        for i in range(len(claster_arr)):
            for j in range(len(claster_arr)):
                if i == j: continue
                if min_dist > dist_matrix[i][j]:
                    min_dist = dist_matrix[i][j]
                    C1 = i
                    C2 = j

        # проверка на пороговое значение
        if t < min_dist: break

        # сливаем два самых близких кластера
        C1_obj = claster_arr[C1]
        C2_obj = claster_arr[C2]
        claster_arr.remove(C1_obj)
        claster_arr.remove(C2_obj)
        for i in range(len(C2_obj.point_arr)):
            C1_obj.point_arr.append(C2_obj.point_arr[i])
        C1_obj.center = vector_mean_value(C1_obj)
        claster_arr.append(C1_obj)

    return claster_arr

# Алгоритм кластеризации по K средним
# M - входное множество образов (двумерный массив, образ*признак)
# k - число кластеров
# Dist_func_F2F - функция для рассчета растояния между образами
# Dist_func_F2С - функция для рассчета растояния между кластером и образом
# функция возвращает массив кластеров
#
# Метод кластеризации по k средним требует задания числа кластеров – k. На первом шаге в пространстве признаков
# произвольно выбирается положение k цен-тров кластеров, не обязательно совпадающих с какими-либо образами.
# Далее на каждом шаге, во-первых, каждый образ относится к тому кластеру, расстояние до центра которого для него
# минимально, а во-вторых, после распределения всех образов по кластерам производится перерасчет положения центров
# кластеров. Процесс продолжается до тех пор, пока не стабилизируется состав кластеров.
# Цель метода – минимизировать суммарное расстояние от центров кластеров до отнесенных к ним образов по всем кластерам.
# Возможно схождение процесса к локальному минимуму, а также отсутствие образов в некоторых кластерах, но,
# изменяя число кластеров и сравнивая результаты, можно найти подходящее число кластеров.
def alg_By_K_mean(M, k, Dist_func_F2F, Dist_func_F2C):
    # инициализируем массив кластеров
    claster_arr = []
    for i in range(k):
        tmp_cl = Claster(M[i])
        tmp_cl.point_arr.append(M[i])
        claster_arr.append(tmp_cl)

    # распологаем все оставшиеся образы по кластерам
    for i in range(k+1, len(M)):
        claster_arr[random.randint(0, k-1)].point_arr.append(M[i])

    # максимальное количество итераций
    limit_iteration = 1000

    # перерасчет положений центров кластеров и перераспределение образов между кластерами
    while 1:
        limit_iteration -= 1
        if limit_iteration == 0: break

        for i in range(k):
            claster_arr[i].center = vector_mean_value(claster_arr[i])

        # перебираем все образы
        flag_break = 0
        for i in range(k):
            for j in range(len(claster_arr[i].point_arr)):
                # находим минимальное растояние от образа до кластера
                min_dist = 999999
                numb_cluster = 0
                # перебираем все кластеры
                for z in range(k):
                    dist = Dist_func_F2C(claster_arr[z], claster_arr[i].point_arr[j], Dist_func_F2F)
                    if (min_dist > dist):
                        min_dist = dist
                        numb_cluster = z

                # если образ ближе к другому кластеру, то переводим его в другой кластер
                if i != numb_cluster:
                    claster_arr[numb_cluster].point_arr.append(claster_arr[i].point_arr[j])
                    claster_arr[i].point_arr.remove(claster_arr[i].point_arr[j])
                    flag_break = 1
                    break
            if flag_break: break

    return claster_arr


# --------------------------------------------------------------
# Служебные функции
# --------------------------------------------------------------

# Функция вывода массива кластеров в консоль
# C - кластер
def Print_Cluster(C_arr):
    print("==============================================")
    print("Number of clusters: ")
    print(len(C_arr))

    for i in range(len(C_arr)):
        print("\tCluster: ", i)
        str_tmp = "\t\tCenter of cluster: "
        for j in range(len(C_arr[i].center)):
            str_tmp += str(C_arr[i].center[j]) + ", "
        print(str_tmp)

        print("\t\tPoint in cluster: ")
        for j in range(len(C_arr[i].point_arr)):
            str_tmp = "["
            for z in range(len(C_arr[i].point_arr[j])):
                str_tmp += str(C_arr[i].point_arr[j][z]) + ", "
            str_tmp += "]"
            print("\t\t\t", str_tmp)




# --------------------------------------------------------------
# Main: Настройка варианта
# --------------------------------------------------------------
# Расстояние между образами: Евклидово и Манхеттенское
# Метод кластерризации: цепной
# Множество образов: (14,3,16), (2,16,1), (-9,-4,3), (18,11,11), (11,15,-12),
# (-1,7,-32), (10,2,18), (7,19,2), (-8,-5,6), (15,14,18), (16,16,-20), (-3,6,-33), (13,6,13), (9,16,4)

M = [[14, 3, 16], [2, 16, 1], [-9, -4, 3], [18, 11, 11], [11, 15, -12], [-1, 7, -32], [10, 2, 18], [7, 19, 2], [-8, -5, 6], [15, 14, 18], [16, 16, -20], [-3, 6, -33], [13, 6, 13], [9, 16, 4]]
t = 20

clasters_cepnoy_Euclide = alg_Cepnoy(M, t, Distance_F2F_Euclide)
print("Cepnoy Euclide:")
Print_Cluster(clasters_cepnoy_Euclide)

clasters_cepnoy_Manshetinscoe = alg_Cepnoy(M, t, Distance_F2F_Manshetinscoe)
print("Cepnoy Mansheten:")
Print_Cluster(clasters_cepnoy_Manshetinscoe)

# Ниже код можно раскоментировать для тестирования программы во всех режимах
# clasters_cepnoy_Dominate = alg_Cepnoy(M, t, Distance_F2F_Dominate)
# print("Cepnoy Dominate:")
# Print_Cluster(clasters_cepnoy_Dominate)
#
# clasters_cepnoy_Camber = alg_Cepnoy(M, t, Distance_F2F_Camberra)
# print("Cepnoy Camber:")
# Print_Cluster(clasters_cepnoy_Camber)
#
# clasters_cepnoy_Cos = alg_Cepnoy(M, t, Distance_F2F_Cosinus)
# print("Cepnoy Cos:")
# Print_Cluster(clasters_cepnoy_Cos)
#
#
# cluster_porogovy_Euclide = alg_Porogovy(M, t, Distance_F2F_Euclide, Distance_F2C_Euclide_with_Cov)
# print("Porogovy Euclide:")
# Print_Cluster(cluster_porogovy_Euclide)
#
# cluster_porogovy_Machalanobis = alg_Porogovy(M, t, Distance_F2F_Euclide, Distance_F2C_Maxalanobis)
# print("Porogovy Mashalanobis:")
# Print_Cluster(cluster_porogovy_Machalanobis)
#
# cluster_porogovy_Near_Manch = alg_Porogovy(M, t, Distance_F2F_Manshetinscoe, Distance_F2C_Near)
# print("Porogovy Near Manch:")
# Print_Cluster(cluster_porogovy_Near_Manch)
#
# cluster_porogovy_Center_Manch = alg_Porogovy(M, t, Distance_F2F_Manshetinscoe, Distance_F2C_Center)
# print("Porogovy Cent Manch:")
# Print_Cluster(cluster_porogovy_Center_Manch)
#
#
# cluster_sliyanie_Euclide_Center = alg_Sliyanie(M, t, Distance_F2F_Euclide, Distance_C2C_Centers, 0)
# print("Cliyanie Euclide Center:")
# Print_Cluster(cluster_sliyanie_Euclide_Center)
#
# cluster_sliyanie_Euclide_Near = alg_Sliyanie(M, t, Distance_F2F_Euclide, Distance_C2C_Near, 0)
# print("Cliyanie Euclide Near:")
# Print_Cluster(cluster_sliyanie_Euclide_Near)
#
# cluster_sliyanie_Euclide_Far = alg_Sliyanie(M, t, Distance_F2F_Euclide, Distance_C2C_Far, 0)
# print("Cliyanie Euclide Far:")
# Print_Cluster(cluster_sliyanie_Euclide_Far)
#
# cluster_sliyanie_Euclide_K = alg_Sliyanie(M, t, Distance_F2F_Euclide, Distance_C2C_K, 10)
# print("Cliyanie Euclide K:")
# Print_Cluster(cluster_sliyanie_Euclide_K)
#
# cluster_K_mean_Euclide_center = alg_By_K_mean(M, t, Distance_F2F_Euclide, Distance_F2C_Center)
# print("By K mean Euclide Center:")
# Print_Cluster(cluster_K_mean_Euclide_center)
#
# cluster_K_mean_Euclide_Euclide = alg_By_K_mean(M, t, Distance_F2F_Euclide, Distance_F2C_Euclide_with_Cov)
# print("By K mean Euclide Euclide:")
# Print_Cluster(cluster_K_mean_Euclide_Euclide)
#
# cluster_K_mean_Euclide_Near = alg_By_K_mean(M, t, Distance_F2F_Euclide, Distance_F2C_Near)
# print("By K mean Euclide Near:")
# Print_Cluster(cluster_K_mean_Euclide_Near)
#
# cluster_K_mean_Euclide_Machalanobis = alg_By_K_mean(M, t, Distance_F2F_Euclide, Distance_F2C_Maxalanobis)
# print("By K mean Euclide Machalanobis:")
# Print_Cluster(cluster_K_mean_Euclide_Machalanobis)