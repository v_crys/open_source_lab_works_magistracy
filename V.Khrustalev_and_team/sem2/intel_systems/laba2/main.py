# ==================================================
# Лабораторная №2 по Интеллектуальным системам
# "Моделирование однослойной нейронной сети"
# ==================================================
import numpy as np
import random

# --------------------------------------------------
# Пути до файлов
# --------------------------------------------------

# пути до эталонных образов
File_NN_etalon = ["./data/etalons/dif/0.nn", "./data/etalons/dif/1.nn", "./data/etalons/dif/V.nn",
                  "./data/etalons/dif/S.nn", "./data/etalons/dif/P.nn"]

File_NN_etalon_one = ["./data/etalons/one/0_0.nn", "./data/etalons/one/0_1.nn", "./data/etalons/one/0_2.nn"]

# пути до проверочных образов (эталонные образы изменены)
File_NN_test = ["./data/tests/t0.nn", "./data/tests/t1.nn", "./data/tests/tV.nn",
                "./data/tests/tS.nn", "./data/tests/tP.nn"]

# пути до нейронов (файлов, которые хранят весовые коэффициенты по каждому нейрону;
# на каждую букву отводится один нейрон)
# Перед обучением эти файлы должны содержать нулевые матрицы 5 * 7
File_NN_neuron = ["./data/weights/n0.nn", "./data/weights/n1.nn", "./data/weights/nV.nn",
                  "./data/weights/nS.nn", "./data/weights/nP.nn"]

# --------------------------------------------------
# Константы
# --------------------------------------------------
Const_Row_number = 7  # количество строк
Const_Column_number = 5  # количество столбцов
Const_different = 1 # если 1, то при обучении используется первое
# обучающее множестов с объектами из различных классов,
# если 0, то используется множестов с объектами из
# одного класса

# --------------------------------------------------
# Структуры
# --------------------------------------------------
class Neuron(object):
    def __init__(self):
        # значение для функции активации (см. методичку x0 = 1)
        self.activate_value = 1
        # все остальные входы нейрона
        self.arr = np.zeros((Const_Row_number, Const_Column_number))


# --------------------------------------------------
# Вспомогательные функции
# --------------------------------------------------

# функция для загрузки файлов
# file_name - путь до файла
# функция возвращает нейрон
def load_file(file_name):
    f = open(file_name, 'r')
    neuron = Neuron()
    i = 0
    for line in f:
        nums = line.split(" ")
        if len(nums) == 1:  # это первая строка с одним элементом x0
            neuron.activate_value = float(nums[0])
            continue

        for j in range(len(nums)):
            if nums[j] == '\n': break
            neuron.arr[i][j] = float(nums[j])
        i += 1

    f.close()
    return neuron


# функция для сохранения в файл
# file_name - путь до файла
# neuron - нейрон для сохранения
# функция ничего не возвращает
def save_file(file_name, neuron):
    f = open(file_name, 'w')
    f.write(str(neuron.activate_value)+"\n")
    for i in range(len(neuron.arr)):
        tmp_str = ""
        for j in range(len(neuron.arr[i])):
            tmp_str += str(neuron.arr[i][j]) + " "
        tmp_str += "\n"
        f.write(tmp_str)
    f.close()


# --------------------------------------------------
# Главные функции
# --------------------------------------------------

# функция распознавания
# form - образ, который необходимо распознать (массив)
# neuro_arr - массив с нейронами
# функция возвращает выходной вектор
def NN_working(form, neuro_arr):
    out_vector = np.zeros(len(File_NN_neuron))

    # вычисляем для каждого нейрона его выходное значение
    for i in range(len(neuro_arr)):
        tmp_sum = 0.0
        # проходим по всем входам нейрона и перемножаем каждый входной элемент на
        # соответствующий вес
        for j in range(len(neuro_arr[i].arr)):
            for z in range(len(neuro_arr[i].arr[j])):
                tmp_sum += neuro_arr[i].arr[j][z] * form.arr[j][z]
        # прогоняем через функцию активации
        if tmp_sum > neuro_arr[i].activate_value:
            out_vector[i] = 1
        else:
            out_vector[i] = 0

    return out_vector


# функция обучения методом коррекции ошибок
# функция выполняет один цикл обучения
# form - тестируемый образ
# etalon_vector - вектор, который соответствует данному образу
# neuro_arr - нейронная сеть (массивы с массивами весовых коэффициентов)
# функция возвращает обновленный neuro_arr (или такой же если ошибки не было)
def NN_learning(form, etalon_vector, neuro_arr):
    # просим у нейронной сети распознать образ
    tmp_vector = NN_working(form, neuro_arr)

    # производим корректировку весов
    # проходим по всем нейронам
    for i in range(len(neuro_arr)):
        # если текущий нейрон вынес правильный вердикт, его не редактируем
        if tmp_vector[i] == etalon_vector[i]:
            continue

        # меняем веса для нейрона
        # дельта показывает, должны ли мы прибавлять или вычитать определенный вес у нейрона
        # идея следующая, если эталонный вектор единица (то есть данный нейрон
        # должен был принять единицу, а не принял, то дельта будет равна 1, что означает
        # что у всех входных коэффициентов, которые были задействованы необходимо прибавить вес на 1
        # как бы усилить их влияние на результат, а если наоборот, еталонное значение 0, но нейрон
        # выдал 1, то наоборот, дельта будет равна -1 и задействованные при этом распознавании веса
        # олжны быть уменьшены, их влияние должно быть уменьшено
        delta = etalon_vector[i] - tmp_vector[i]
        # проходим все входы и меняем те, которые были задействованы при текущем распознавании в
        # соответствии с дельтой
        for x in range(len(neuro_arr[i].arr)):
            for y in range(len(neuro_arr[i].arr[x])):
                neuro_arr[i].arr[x][y] += form.arr[x][y] * delta

        # не забываем поменять у нейрона значение функции активации
        neuro_arr[i].activate_value += delta  # помним, что нулевой вход x0 = 1 всегда
    return neuro_arr


# --------------------------------------------------
# Основное тело программы main
# --------------------------------------------------

# загружаем все образы из файлов
number_of_neurons = len(File_NN_etalon)  # количество нейронов
forms_etalon = []
if Const_different:
    # загружаем различные образы
    number_of_etalons = len(File_NN_etalon)
    for i in range(number_of_etalons):
        forms_etalon.append(load_file(File_NN_etalon[i]))

else:
    # загружаем образы из одной категории
    number_of_etalons = len(File_NN_etalon_one)
    for i in range(number_of_etalons):
        forms_etalon.append(load_file(File_NN_etalon_one[i]))

forms_test = []
for i in range(number_of_neurons):
    forms_test.append(load_file(File_NN_test[i]))

# создаем массив корректных выходных векторов
answer_arr = []

for i in range(number_of_neurons):
    tmp_arr = np.zeros(number_of_neurons)
    tmp_arr[i] = 1
    answer_arr.append(tmp_arr)


# загружаем из файлов весовые коэффициенты
neuro_arr = []
for i in range(number_of_neurons):
    neuro_arr.append(load_file(File_NN_neuron[i]))

# обучаем нейронную сеть
iteration_counter = 0  # счетчик итераций обучения
while 1:
    iteration_counter += 1
    # Выбираем случайный эталонный образ для обучения
    numb_form = random.randint(0, number_of_etalons - 1)

    # производим одну итерацию обучения
    neuro_arr = NN_learning(forms_etalon[numb_form], answer_arr[numb_form], neuro_arr)

    # проверяем, обучилась ли нейронная сеть (все образы
    # распознаются правильно)
    flag_NN_learned = 1
    # перебираем все образы
    for i in range(number_of_etalons):
        # если для текущего образа сеть приняла неправильное решение
        # то помечам флаг и идем на следующую итерацию
        NN_res = NN_working(forms_etalon[i], neuro_arr)
        for j in range(len(answer_arr[i])):
            if NN_res[j] != answer_arr[i][j]:
                flag_NN_learned = 0
                break
        if flag_NN_learned == 0:
            break

    # если сеть обучилась, выходим из обучения
    if flag_NN_learned:
        break

print("Iterate counter: " + str(iteration_counter))

# Сохраняем весовые коэффициенты, полученные при обучении
for i in range(number_of_neurons):
    save_file(File_NN_neuron[i], neuro_arr[i])

# проверяем нейронную сеть на тестовых примерах
# данные примеры не входят в обучающую выборку
for i in range(len(forms_test)):
    print("Test #" + str(i) + " success: ")
    NN_res = NN_working(forms_test[i], neuro_arr)
    flag_correct = 1
    for j in range(len(answer_arr[i])):
        if NN_res[j] != answer_arr[i][j]:
            flag_correct = 0
            break

    if flag_correct:
        print("\tYES")
    else:
        print("\tNO")

