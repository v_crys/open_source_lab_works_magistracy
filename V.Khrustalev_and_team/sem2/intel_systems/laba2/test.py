[
    {
        "Name": "Забыли включить питание",
        "Recommendation": "Воткнуть вилку в розетку и попробовать снова",
        "Pa": 0.2,
        "sym_arr": [
            {
                "Numb": 4,
                "P": 0.3,
                "Pno": 0.8
            },
            {
                "Numb": 5,
                "P": 0.25,
                "Pno": 0.6
            },
            {
                "Numb": 6,
                "P": 0.1,
                "Pno": 0.95
            },
            {
                "Numb": 0,
                "P": 0.1,
                "Pno": 0.95
            },
            {
                "Numb": 1,
                "P": 0.1,
                "Pno": 0.95
            }
        ]
    },
    {
        "Name": "Компьютер грязный",
        "Recommendation": "Почистить компьютер, смазать вентиляторы, поменять термопасту",
        "Pa": 0.1,
        "sym_arr": [
            {
                "Numb": 8,
                "P": 0.5,
                "Pno": 0.3
            },
            {
                "Numb": 9,
                "P": 0.3,
                "Pno": 0.6
            },
            {
                "Numb": 12,
                "P": 0.7,
                "Pno": 0.4
            },
            {
                "Numb": 13,
                "P": 0.7,
                "Pno": 0.4
            },
            {
                "Numb": 5,
                "P": 0.15,
                "Pno": 0.9
            }
        ]
    },
    {
        "Name": "Выгорел USB порт",
        "Recommendation": "Переключить внешние устройства в другой порт",
        "Pa": 0.05,
        "sym_arr": [
            {
                "Numb": 1,
                "P": 0.5,
                "Pno": 0.6
            },
            {
                "Numb": 3,
                "P": 0.5,
                "Pno": 0.5
            },
            {
                "Numb": 0,
                "P": 0.45,
                "Pno": 0.75
            },
            {
                "Numb": 8,
                "P": 0.1,
                "Pno": 0.95
            },
            {
                "Numb": 10,
                "P": 0.05,
                "Pno": 0.99
            }
        ]
    },
    {
        "Name": "Подключили монитор не в тот порт",
        "Recommendation": "Переподключить монитор в правильный порт",
        "Pa": 0.1,
        "sym_arr": [
            {
                "Numb": 4,
                "P": 0.9,
                "Pno": 0.3
            },
            {
                "Numb": 5,
                "P": 0.3,
                "Pno": 0.8
            },
            {
                "Numb": 16,
                "P": 0.7,
                "Pno": 0.8
            },
            {
                "Numb": 11,
                "P": 0.2,
                "Pno": 0.9
            },
            {
                "Numb": 10,
                "P": 0.1,
                "Pno": 0.9
            }
        ]
    },
    {
        "Name": "Вышла из строя ОЗУ",
        "Recommendation": "Поменять ОЗУ",
        "Pa": 0.04,
        "sym_arr": [
            {
                "Numb": 5,
                "P": 0.4,
                "Pno": 0.8
            },
            {
                "Numb": 6,
                "P": 0.3,
                "Pno": 0.8
            },
            {
                "Numb": 10,
                "P": 0.2,
                "Pno": 0.9
            },
            {
                "Numb": 11,
                "P": 0.35,
                "Pno": 0.8
            },
            {
                "Numb": 9,
                "P": 0.4,
                "Pno": 0.8
            }
        ]
    },
    {
        "Name": "Вышел из строя жесткий диск",
        "Recommendation": "Поменять жесткий диск",
        "Pa": 0.01,
        "sym_arr": [
            {
                "Numb": 5,
                "P": 0.4,
                "Pno": 0.7
            },
            {
                "Numb": 6,
                "P": 0.35,
                "Pno": 0.8
            },
            {
                "Numb": 8,
                "P": 0.2,
                "Pno": 0.9
            },
            {
                "Numb": 9,
                "P": 0.15,
                "Pno": 0.9
            },
            {
                "Numb": 14,
                "P": 0.5,
                "Pno": 0.6
            }
        ]
    },
    {
        "Name": "Компьютер завирусован",
        "Recommendation": "Почистить антивирусом или переустановить систему",
        "Pa": 0.3,
        "sym_arr": [
            {
                "Numb": 19,
                "P": 0.8,
                "Pno": 0.1
            },
            {
                "Numb": 11,
                "P": 0.4,
                "Pno": 0.8
            },
            {
                "Numb": 6,
                "P": 0.5,
                "Pno": 0.6
            },
            {
                "Numb": 0,
                "P": 0.3,
                "Pno": 0.8
            },
            {
                "Numb": 1,
                "P": 0.3,
                "Pno": 0.8
            }
        ]
    },
    {
        "Name": "Не в то гнездо подключили колонки",
        "Recommendation": "Переподключить колонки",
        "Pa": 0.1,
        "sym_arr": [
            {
                "Numb": 2,
                "P": 0.9,
                "Pno": 0.2
            },
            {
                "Numb": 6,
                "P": 0.1,
                "Pno": 0.95
            },
            {
                "Numb": 5,
                "P": 0.05,
                "Pno": 0.99
            },
            {
                "Numb": 10,
                "P": 0.1,
                "Pno": 0.95
            },
            {
                "Numb": 8,
                "P": 0.02,
                "Pno": 0.99
            }
        ]
    },
    {
        "Name": "Проблема в совместимости элементов системного блока",
        "Recommendation": "Проверить совместимость",
        "Pa": 0.05,
        "sym_arr": [
            {
                "Numb": 16,
                "P": 0.1,
                "Pno": 0.95
            },
            {
                "Numb": 17,
                "P": 0.1,
                "Pno": 0.95
            },
            {
                "Numb": 18,
                "P": 0.1,
                "Pno": 0.95
            },
            {
                "Numb": 6,
                "P": 0.7,
                "Pno": 0.4
            },
            {
                "Numb": 8,
                "P": 0.25,
                "Pno": 0.8
            }
        ]
    },
    {
        "Name": "Сгорела материнская плата",
        "Recommendation": "Поменять мат. плату",
        "Pa": 0.05,
        "sym_arr": [
            {
                "Numb": 5,
                "P": 0.95,
                "Pno": 0.1
            },
            {
                "Numb": 15,
                "P": 0.9,
                "Pno": 0.1
            },
            {
                "Numb": 16,
                "P": 0.1,
                "Pno": 0.95
            },
            {
                "Numb": 17,
                "P": 0.1,
                "Pno": 0.95
            },
            {
                "Numb": 18,
                "P": 0.1,
                "Pno": 0.95
            }
        ]
    }
]