#!/bin/bash

vcs -full64 -sverilog -debug_all +vcs+flush+fopen   \
    -timescale=1ns/1ps                              \
    +incdir+./                                      \
    ./mil_std_1553b_main.sv

./simv -gui