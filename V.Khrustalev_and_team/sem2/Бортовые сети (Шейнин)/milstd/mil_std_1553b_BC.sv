
`ifndef MIL_STD_BC_DEF
`define MIL_STD_BC_DEF

class mil_std_1553b_BC extends mil_std_1553b_low_level;

    function new();
        addr_slv = '1;
    endfunction : new

    task gen_BC_RT(mil_std_1553b_transaction tr);
        mil_std_type_word_e     type_word;
        logic [15 : 0]          command_word;
        int                     len = (tr.len_transaction == '0) ? 'd32 : tr.len_transaction;

        command_word     =  {
                                tr.terminal_addr,
                                logic'(MIL_STD_DIR_RT_IS_RECEIVE),
                                tr.subaddress_mode,
                                tr.len_transaction
                            };

        // generation command word
        gen_word(MIL_STD_COMMAND_WORD, command_word);

        // generation data
        for (int i = 0; i < len; i++) begin
            gen_word(MIL_STD_DATA_WORD, tr.data[i]);
        end

        // wait status
        wait_word(type_word, tr.status, tr.parity_correct); // MIL_STD_COMMAND_WORD
    endtask : gen_BC_RT

    task gen_RT_BC(mil_std_1553b_transaction tr);
        mil_std_type_word_e     type_word;
        logic [15 : 0]          command_word;
        logic                   parity_data;
        int                     len = (tr.len_transaction == '0) ? 'd32 : tr.len_transaction;

        command_word     =  {
                                tr.terminal_addr,
                                logic'(MIL_STD_DIR_RT_IS_TRANSMIT),
                                tr.subaddress_mode,
                                tr.len_transaction
                            };

        // generation command word
        gen_word(MIL_STD_COMMAND_WORD, command_word);

        // wait status
        wait_word(type_word, tr.status, tr.parity_correct); // MIL_STD_COMMAND_WORD

        // generation data
        for (int i = 0; i < len; i++) begin
            wait_word(type_word, tr.data[i], parity_data); // MIL_STD_DATA_WORD
            tr.parity_correct &= parity_data;
        end
    endtask : gen_RT_BC

    task gen_RT_RT(mil_std_1553b_transaction tr);
        logic [15:0]            status;
        mil_std_type_word_e     type_word;
        logic [15 : 0]          trans_command_word;
        logic [15 : 0]          receiv_command_word;
        logic [15 : 0]          data;
        logic                   parity_correct;
        int                     len = (tr.len_transaction == '0) ? 'd32 : tr.len_transaction;

        trans_command_word     =  {
                                tr.terminal_addr,
                                logic'(MIL_STD_DIR_RT_IS_TRANSMIT),
                                tr.subaddress_mode,
                                tr.len_transaction
                            };

        receiv_command_word     =  {
                                tr.addr2,
                                logic'(MIL_STD_DIR_RT_IS_RECEIVE),
                                tr.subaddress_mode,
                                tr.len_transaction
                            };

        // generation command word
        gen_word(MIL_STD_COMMAND_WORD, receiv_command_word);
        gen_word(MIL_STD_COMMAND_WORD, trans_command_word);

        /// wait status response, data, and status response

        // wait status
        wait_word(type_word, status, parity_correct); // MIL_STD_COMMAND_WORD

        // generation data
        for (int i = 0; i < len; i++)
            wait_word(type_word, data, parity_correct); // MIL_STD_DATA_WORD

        // wait status
        wait_word(type_word, status, parity_correct); // MIL_STD_COMMAND_WORD

    endtask : gen_RT_RT

endclass : mil_std_1553b_BC

`endif // MIL_STD_BC_DEF