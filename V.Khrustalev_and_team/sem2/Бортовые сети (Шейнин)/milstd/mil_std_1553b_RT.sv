
`ifndef MIL_STD_RT_DEF
`define MIL_STD_RT_DEF

class mil_std_1553b_RT extends mil_std_1553b_low_level;

    logic [15:0] memory [32*16 - 1:0];
    logic [4:0]  RT_addr;
    bit          flag_RT_RT_tx;

    function new(logic [4:0] addr);
        RT_addr     = addr;
        addr_slv    = addr;
        foreach (memory[i]) memory[i] = $urandom;
        flag_RT_RT_tx = 0;
    endfunction : new

    task gen_response();
        logic [15:0]                word;
        mil_std_command_word_s      cmd_word;
        logic                       parity;
        mil_std_status_word_s       status_word;
        mil_std_type_word_e         type_word;
        mil_std_command_word_s      cmd_word_transmit;

        wait_word(type_word, word, parity); // MIL_STD_COMMAND_WORD
        cmd_word = mil_std_command_word_s'(word);

        // check start transaction
        if (type_word == MIL_STD_DATA_WORD) begin
            flag_RT_RT_tx = 0;
            return;
        end
        // transaction not for current RT
        if (cmd_word.remote_addr != RT_addr) begin
            flag_RT_RT_tx = 1;
            return;
        end

        // if transaction for current slave, then check mode
        if (cmd_word.direction == MIL_STD_DIR_RT_IS_RECEIVE) begin
            // BC-RT or RT-RT transaction
            wait_word(type_word, word, parity);

            if (type_word == MIL_STD_COMMAND_WORD) begin
                // RT-RT receiver
                cmd_word_transmit = mil_std_command_word_s'(word);
                $display("%h: Detected RT_RT transaction receiver: %h -> %h", RT_addr, cmd_word_transmit.remote_addr, cmd_word.remote_addr);

                wait_word(type_word, word, parity); // wait status from RT transmiter
                for (int i = 0; i < cmd_word.len_transaction; i++) begin
                    wait_word(type_word, memory[cmd_word.subaddress_mode * 'd16 + i], parity);
                end
                status_word = $urandom();
                status_word.remote_addr = RT_addr;
                gen_word(MIL_STD_COMMAND_WORD, status_word); // send status
            end else begin
                // BC-RT
                $display("%h: Detected BC_RT transaction", RT_addr);
                memory[cmd_word.subaddress_mode * 'd16] = word;
                for (int i = 1; i < cmd_word.len_transaction; i++) begin
                    wait_word(type_word, memory[cmd_word.subaddress_mode * 'd16 + i], parity);
                end
                status_word = $urandom();
                status_word.remote_addr = RT_addr;
                gen_word(MIL_STD_COMMAND_WORD, status_word); // send status
            end
        end else begin
            // RT-BC transaction (and RT transmitter)

            if (flag_RT_RT_tx)
                $display("%h: Detected RT_RT transaction transmiter", RT_addr);
            else
                $display("%h: Detected RT_BC transaction", RT_addr);

            @(test_MIL_STD.clk);
            @(test_MIL_STD.clk);

            status_word = $urandom();
            status_word.remote_addr = RT_addr;
            gen_word(MIL_STD_COMMAND_WORD, status_word); // send status

            for (int i = 0; i < cmd_word.len_transaction; i++) begin
                gen_word(MIL_STD_DATA_WORD, memory[cmd_word.subaddress_mode * 'd16 + i]); // send data
            end

            if (flag_RT_RT_tx) begin
                // parse response from RT reseiver
                wait_word(type_word, word, parity);
                flag_RT_RT_tx = 0;
            end
        end
    endtask : gen_response

endclass : mil_std_1553b_RT

`endif //  MIL_STD_RT_DEF