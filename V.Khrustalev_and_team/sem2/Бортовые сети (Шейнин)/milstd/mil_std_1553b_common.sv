
`ifndef MIL_STD_COMMON_DEF
`define MIL_STD_COMMON_DEF

typedef enum logic {
    MIL_STD_DIR_RT_IS_RECEIVE   = 1'b0,
    MIL_STD_DIR_RT_IS_TRANSMIT  = 1'b1
} mil_std_direction_e;

typedef enum logic {
    MIL_STD_COMMAND_WORD    = 1'b0,
    MIL_STD_DATA_WORD       = 1'b1
} mil_std_type_word_e;

typedef struct packed {
    logic       [4:0]       remote_addr;
    mil_std_direction_e     direction;
    logic       [4:0]       subaddress_mode;
    logic       [4:0]       len_transaction;
} mil_std_command_word_s;

typedef struct packed {
    logic       [4:0]       remote_addr;
    logic                   msg_error;
    logic                   instrumentation;
    logic                   service_req;
    logic       [2:0]       reserved;
    logic                   broadcast;
    logic                   busy;
    logic                   subsystem_flag;
    logic                   dyn_ctrl;
    logic                   terminal_flag;
} mil_std_status_word_s;

`endif // MIL_STD_COMMON_DEF