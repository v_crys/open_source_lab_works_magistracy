
`ifndef MIL_STD_LL_DEF
`define MIL_STD_LL_DEF

class mil_std_1553b_low_level;

    logic [4:0] addr_slv;

    function new();

    endfunction : new

    task gen_sync_C();
        @(posedge test_MIL_STD.clk);
        @(posedge test_MIL_STD.clk);
        test_MIL_STD.bus  <= 1'b1;
        @(posedge test_MIL_STD.clk);
        @(negedge test_MIL_STD.clk);
        test_MIL_STD.bus  <= 1'b0;
        @(negedge test_MIL_STD.clk);
        @(posedge test_MIL_STD.clk);

        test_MIL_STD.bus  <= 1'bz;
        $display("%h: gen SYNC_C", addr_slv);
    endtask : gen_sync_C

    task gen_sync_D();
        @(posedge test_MIL_STD.clk);
        @(posedge test_MIL_STD.clk);
        test_MIL_STD.bus  <= 1'b0;
        @(posedge test_MIL_STD.clk);
        @(negedge test_MIL_STD.clk);
        test_MIL_STD.bus  <= 1'b1;
        @(negedge test_MIL_STD.clk);
        @(posedge test_MIL_STD.clk);

        test_MIL_STD.bus  <= 1'bz;
        $display("%h: gen SYNC_D", addr_slv);
    endtask : gen_sync_D

    task gen_bit_0();
        test_MIL_STD.bus  <= 1'b0;
        @(test_MIL_STD.clk);
        test_MIL_STD.bus  <= 1'b1;
        @(test_MIL_STD.clk);
        test_MIL_STD.bus  <= 1'bz;
        //$display("%h: gen bit 0", addr_slv);
    endtask : gen_bit_0

    task gen_bit_1();
        test_MIL_STD.bus  <= 1'b1;
        @(test_MIL_STD.clk);
        test_MIL_STD.bus  <= 1'b0;
        @(test_MIL_STD.clk);
        test_MIL_STD.bus  <= 1'bz;
        //$display("%h: gen bit 1", addr_slv);
    endtask : gen_bit_1

    task gen_word(input mil_std_type_word_e type_word, input logic [15:0] data);
        // set on the test_MIL_STD.bus command or data word start signal (3 Clk)
        if (type_word == MIL_STD_COMMAND_WORD) begin
            gen_sync_C();
        end else begin
            gen_sync_D();
        end

        // set on the test_MIL_STD.bus 16 bit (milstd word) (16 clk)
        for (int i = 0; i < 16; i++) begin
            if (data[i] == 1'b0) begin
                gen_bit_0();
            end else begin
                gen_bit_1();
            end
        end

        // inversity parity (1 Clk)
        if (~(^data)) begin
            gen_bit_1();
        end else begin
            gen_bit_0();
        end

        $display("%h: gen word: %h", addr_slv, data);
    endtask : gen_word

    task wait_sync_C();
        wait (test_MIL_STD.bus == 0);
        @(test_MIL_STD.clk);
        @(test_MIL_STD.clk);
        @(test_MIL_STD.clk);

        $display("%h: detected sync C", addr_slv);
    endtask : wait_sync_C

    task wait_sync_D();
        wait (test_MIL_STD.bus);
        @(test_MIL_STD.clk);
        @(test_MIL_STD.clk);
        @(test_MIL_STD.clk);

        $display("%h: detected sync D", addr_slv);
    endtask : wait_sync_D

    task wait_sync(output mil_std_type_word_e type_sync);
        #1;
        wait (test_MIL_STD.bus !== 'z);

        if (test_MIL_STD.bus == '1) begin
            wait_sync_C();
            type_sync = MIL_STD_COMMAND_WORD;
        end else begin
            wait_sync_D();
            type_sync = MIL_STD_DATA_WORD;
        end
    endtask : wait_sync

    task wait_bit(output logic value);
        #1;
        value = test_MIL_STD.bus;
        @(test_MIL_STD.clk);
        @(test_MIL_STD.clk);

        //$display("%h: detected bit: %h", addr_slv, value);
    endtask : wait_bit

    task wait_word(
        output mil_std_type_word_e  type_word,
        output logic [15:0]         data,
        output logic                parity_correct
        );
        logic value;

        // wait synchro start
        wait_sync(type_word);

        // wait 16 bit
        for (int i = 0; i < 16; i++) begin
            wait_bit(value);
            data[i] = value;
        end

        // wait parity
        wait_bit(value);
        parity_correct = value ^ (^data);

        $display("%h: detected word: %h", addr_slv, data);
    endtask : wait_word

endclass : mil_std_1553b_low_level

`endif //  MIL_STD_LL_DEF