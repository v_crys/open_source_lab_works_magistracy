`include "mil_std_1553b_common.sv"
`include "mil_std_1553b_transaction.sv"
`include "mil_std_1553b_low_level.sv"
`include "mil_std_1553b_BC.sv"
`include "mil_std_1553b_RT.sv"

module test_MIL_STD();

logic       clk;
logic       bus;

mil_std_1553b_BC    mil_BC;
mil_std_1553b_RT    mil_RT_1;
mil_std_1553b_RT    mil_RT_2;

initial begin
    clk = 0;
    bus = 'z;
    forever #10 clk = !clk;
end

initial begin
    // connect interface
    mil_BC      = new();
    mil_RT_1    = new(5'b0); // 5'b0 - is address
    mil_RT_2    = new(5'b1); // 5'b1 - is address
end

initial begin
    // generation random mil_std_transaction (simulation BC)
    mil_std_1553b_transaction gen_trans;
    gen_trans = new();

    // generation 10 transaction RT_RT
    for (int i = 0; i < 10; i++) begin
        assert(gen_trans.randomize()
         with {
                terminal_addr   <=  1'b1;           // constraint - generate only transaction with address 0 and 1
                addr2           <=  1'b1;           // constraint - generate only transaction with address 0 and 1
                addr2           !=  terminal_addr;  // constraint - source RT and destination RT dont equal
                len_transaction <   30;             // constraint - not generate broadcast transaction
                len_transaction >   0;
            });
        mil_BC.gen_RT_RT(gen_trans);
        gen_trans.print();
    end

    #1000;

    // generation 10 transaction RT_BC
    for (int i = 0; i < 10; i++) begin
        assert(gen_trans.randomize() with {
                terminal_addr       <= 1'b1;    // constraint - generate only transaction with address 0 and 1
                len_transaction     < 30;       // constraint - not generate broadcast transaction
                len_transaction     > 0;
            });
        mil_BC.gen_RT_BC(gen_trans);
        gen_trans.print();
    end

    #1000;

    // generation 10 transaction BC_RT
    for (int i = 0; i < 10; i++) begin
        assert(gen_trans.randomize() with {
                terminal_addr       <= 1'b1;    // constraint - generate only transaction with address 0 and 1
                len_transaction     < 30;       // constraint - not generate broadcast transaction
                len_transaction     > 0;
            }
        );
        mil_BC.gen_BC_RT(gen_trans);
        gen_trans.print();
    end

    $display("Simulation complete...");
    $finish;
end

initial begin
    // answer on the transactions (simulation RT_1)
    forever begin
        mil_RT_1.gen_response();
    end
end

initial begin
    // answer on the transactions (simulation RT_2)
    forever begin
        mil_RT_2.gen_response();
    end
end

endmodule : test_MIL_STD