
`ifndef MIL_STD_TRANSACTION_DEF
`define MIL_STD_TRANSACTION_DEF

class mil_std_1553b_transaction;

    rand logic [4:0]     terminal_addr      ;
    rand logic [4:0]     addr2              ;
    rand logic [4:0]     subaddress_mode    ;
    rand logic [4:0]     len_transaction    ;
    rand logic [15:0]    data[31:0]         ;
    rand logic [15:0]    status             ;
    rand logic           parity_correct     ;

    function new();

    endfunction : new

    function void print();
        $display("Terminal_addr\t= %h\naddr2\t\t= %h\nsubaddress_mode\t= %h\nlen_transaction\t= %h\nstatus\t\t= %h\nparity_correct\t= %h\n",
                terminal_addr,
                addr2,
                subaddress_mode,
                len_transaction,
                status,
                parity_correct
            );

        $display("DATA = ");
        foreach (data[i]) $display("%h", data[i]);
    endfunction

    function void copy(mil_std_1553b_transaction rhs);
        this.terminal_addr      = rhs.terminal_addr;
        this.addr2              = rhs.addr2;
        this.subaddress_mode    = rhs.subaddress_mode;
        this.len_transaction    = rhs.len_transaction;
        this.data               = rhs.data;
        this.status             = rhs.status;
        this.parity_correct      = rhs.parity_correct;
    endfunction : copy

    virtual function mil_std_1553b_transaction clone();
        clone = new();
        clone.copy(this);
    endfunction : clone

endclass : mil_std_1553b_transaction

`endif // MIL_STD_TRANSACTION_DEF