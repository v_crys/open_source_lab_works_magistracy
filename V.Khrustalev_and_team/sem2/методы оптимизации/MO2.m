syms x1 x2
L = (x1^2 + 4)^(1/2) + ((x2 - x1)^2 + 16)^(1/2) + ((x2 - 5)^2 + 9)^(1/2);   % ������� �������

dLx1 = diff(L, x1); % 1 ����������� �� x1
dLx2 = diff(L, x2); % 2 ����������� �� x2
disp('1 ����������� �� x1:'); 
disp(dLx1);
disp('1 ����������� �� x2:'); 
disp(dLx2);

sp = solve(dLx1, dLx2); % ������� ������� ���������
disp('���������� ������������ �����:');
x1p=sp.x1;  x2p=sp.x2;  % ���������� ������������ �����
disp(x1p); disp(x2p);

H = hessian(L, [x1, x2]); % ������� 
disp('������� �������:');
disp(H);
H = int(H);  % ��������� ���� ���������� H 
sg = sign([H(1,1),det(H)]); % ����� �������
disp('����� �������:');
disp(sg(1));
disp(sg(2));

% ����������� ���������� ���������� �����
figure;
x = [0, x1p, x2p, 5];
y = [2, 0, 4, 1];
alph = ['A', 'C', 'D', 'B'];
plot(x, y, 'blue', x, y, 'b*');
title('���� �����');
xlabel('����� ����');
text(2.1, 3.9, '̸����� ����');
for i = 1:1:4
    text(x(i)+0.1, y(i), alph(i));
end

[x1, x2] = meshgrid([0:0.1:5]);
L = (x1.^2 + 4).^(1/2) + ((x2 - x1).^2 + 16).^(1/2) + ((x2 - 5).^2 + 9).^(1/2);
figure;
mesh(x1, x2, L); % ���������� �����������
title('����������� L');
xlabel('x_1');
ylabel('x_2');
zlabel('L');

figure;
contour(x1, x2, L, 'ShowText','on'); % ���������� ����� ������
hold on
scatter(x1p, x2p, 'blue', 'filled');
text(x1p, x2p, '   ���������');
hold off

% %%%%%%%% ����� ��������������� ������
% start_time = cputime;
% e = 0.001;
% % 1 step
% x_0_i = [0, 2]; % ������
% x_i = x_0_i; % ����� �����
% p = x_i;
% n = 2;
% k = 0; % 2 step
% e_i = [0, 0];
% flag = 0; % ����
% t = 0; % ���� ��������� ����������
% iterations = 1; % ����� ��������
% optimisations = 0; % ����� ���������� �����������
% 
% figure;
% contour(x1, x2, L, 'ShowText','on'); % ���������� ����� ������
% title('����� �������� ������� ��������������� ������');
% hold on
% 
% disp('-----------');
% disp('-----------iteration:');
% disp(iterations);
% 
% while(t ~= 1)
%     plot([x_0_i(1),p(1)],[x_0_i(2),p(2)], 'blue');
%     % 3 step
%     k = k + 1;
%     % 4 step
%     if(k == 1)
%         x_i(1) = fminbnd(@(x1)(x1.^2 + 4).^(1/2) + ((x_0_i(2) - x1).^2 + 16).^(1/2) + ((x_0_i(2) - 5).^2 + 9).^(1/2),0,5);
%         optimset('TolX', e);
%         p = x_i;
%         optimisations = optimisations + 1;
%     end
%     
%     if(k == 2)
%         x_i(2) = fminbnd(@(x2)(x_i(1).^2 + 4).^(1/2) + ((x2 - x_i(1)).^2 + 16).^(1/2) + ((x2 - 5).^2 + 9).^(1/2),0,5);
%         optimset('TolX', e);
%         optimisations = optimisations + 1;
%     end
%     
%     % 5 step
%     for i = 1:n
%         e_i(i) = abs(x_i(i) - x_0_i(i));
%     end
%     
%     disp('-----------');
%     disp('x_0:');
%     disp(x_0_i);
%     disp('x_k:');
%     disp(x_i);
%     disp('k:');
%     disp(k);
%     disp('e_k:');
%     disp(e_i);
%     
%     % 6 step
%     if (k < n)
%         continue;
%     else
%         % 7 step
%         for i = 1:n
%             if (e_i(i) < e)
%                 flag = flag + 1;
%             end
%         end
%     
%         if (flag == 2)
%             t = 1;
%         else
%             % 2 step
%             k = 0;
%             flag = 0;
%             x_0_i(1) = x_i(1);
%             x_0_i(2) = x_i(2);
%             iterations = iterations + 1;
%             disp('-----------');
%             disp('-----------iteration:');
%             disp(iterations);
%         end
%     end
% end
% hold off
% end_time = cputime - start_time;
% disp('optimisations:');
% disp(optimisations);
% disp('min');
% disp(x_i);
% f = (x_i(1)^2 + 4)^(1/2) + ((x_i(2) - x_i(1))^2 + 16)^(1/2) + ((x_i(2) - 5)^2 + 9)^(1/2);
% disp(f);
% disp('time');
% disp(end_time);

%%%%%%%% ����� ����-������
start_time = cputime;
e = 0.001;
% 1 step
x_0_i = [0, 2]; % ������
x_i = x_0_i; % ����� �����
p = x_i;
n = 2;
k = 0; % 2 step��
e_i = [0, 0];
flag = 0; % ����
t = 0; % ���� ��������� ����������
iterations = 1; % ����� ��������
optimisations = 0; % ����� ���������� �����������
b = 0; % ��� ���������� ������ �� �������

figure;
contour(x1, x2, L, 'ShowText','on'); % ���������� ����� ������
title('����� �������� ������� ����-������');
hold on

disp('-----------');
disp('-----------iteration:');
disp(iterations);

while(t ~= 1)
    %plot([x_0_i(1),p(1)],[x_0_i(2),p(2)], 'blue');
    % 3 step
    k = k + 1;
    % 4 step
    if(k == 1)
        x_i(1) = fminbnd(@(x1)(x1.^2 + 4).^(1/2) + ((x_0_i(2) - x1).^2 + 16).^(1/2) + ((x_0_i(2) - 5).^2 + 9).^(1/2),0,5);
        optimset('TolX', e);
        p = x_i;
        plot([x_0_i(1),p(1)],[x_0_i(2),p(2)], 'blue');
        optimisations = optimisations + 1;
    end
    
    if(k == 2)
        x_i(2) = fminbnd(@(x2)(x_i(1).^2 + 4).^(1/2) + ((x2 - x_i(1)).^2 + 16).^(1/2) + ((x2 - 5).^2 + 9).^(1/2),0,5);
        optimset('TolX', e);
        plot([p(1),x_i(1)],[p(2),x_i(2)], 'blue');
        optimisations = optimisations + 1;
    end
    
    % 5 step
    for i = 1:n
        e_i(i) = abs(x_i(i) - x_0_i(i));
    end
    
    disp('-----------');
    disp('x_0:');
    disp(x_0_i);
    disp('x_k:');
    disp(x_i);
    disp('k:');
    disp(k);
    disp('e_k:');
    disp(e_i);
    
    % 6 step
    if (k < n)
        continue;
    else
        % 7 step
        for i = 1:n
            if (e_i(i) < e)
                flag = flag + 1;
            end
        end
    
        if (flag == 2)
            t = 1;
        else
            b = fminsearch(@(b)((x_i(1) + b*(x_i(1) - x_0_i(1))).^2 + 4).^(1/2) + (((x_i(2) + b*(x_i(2) - x_0_i(2))) - (x_i(1) + b*(x_i(1) - x_0_i(1)))).^2 + 16).^(1/2) + ((x_i(2) + b*(x_i(2)-x_0_i(2)) - 5).^2 + 9).^(1/2),1,optimset('TolX', e));
            optimisations = optimisations + 1;
            disp('b =');
            disp(b);
            
            % 2 step
            k = 0;
            flag = 0;
            x11 = x_i(1) + b*(x_i(1)- x_0_i(1));
            x22 = x_i(2) + b*(x_i(2)- x_0_i(2));
            plot([x_i(1), x11], [x_i(2), x22], 'magenta');
            x_0_i(1) = x11;
            x_0_i(2) = x22;
            x_i = x_0_i;
            iterations = iterations + 1;
            disp(optimisations);
            disp('-----------');
            disp('-----------iteration:');
            disp(iterations);
        end
    end
end
hold off
end_time = cputime - start_time;
disp('optimisations:');
disp(optimisations);
disp('min');
disp(x_i);
f = (x_i(1)^2 + 4)^(1/2) + ((x_i(2) - x_i(1))^2 + 16)^(1/2) + ((x_i(2) - 5)^2 + 9)^(1/2);
disp(f);
disp('time');
disp(end_time);




