import numpy as np
import math
import matplotlib.pyplot as plt
import time


#                                   построение графика функции
x = np.arange(0, np.pi/2, 0.01*np.pi)
y = np.zeros(len(x))
i = 0
for i in range(len(x)-1):
    y[i] = 15*(2.25 - math.sin(x[i]))/(math.cos(x[i]))
plt.plot(x[0:len(x)-1],y[0:len(x)-1], color = 'green')

t1 = time.time()  
#####################
#                                   метод золотого сечения (222)
a = 0
b = np.pi/2
e = 0.01
F = (1 + math.sqrt(5))/2
tt = 2
ttt = 4
y_i = np.zeros(4)
t = 0
x_min = 0
i = 0
n = 0
N = 0
x = np.zeros(50)

# step 1
y_i[0] = 15*(2.25 - np.sin(a))/(np.cos(a))
y_i[3] = 15*(2.25 - np.sin(b))/(np.cos(b))
N = N + 2  
print("\n")
j = 0
while (t < 1):
    if(tt == 2):
        # step 2  
        c = b - (b - a)/F
        d = b-(c-a)

    # step 3
    y_i[1] = 15*(2.25 - np.sin(c))/(np.cos(c))
    y_i[2] = 15*(2.25 - np.sin(d))/(np.cos(d))
    N = N + 2  
    print("\n")

    if(ttt == 4):
        # step 4
        y_min = np.min(y_i)

    i = 0
    for i in range(len(y_i) - 1):
        if(y_i[i] == y_min):
            x_min = i
    
    if (abs(b - a) < e):
        # step 5
        if(x_min == 0):
            print(" x_min = ", a)
            x_min = a
        if(x_min == 3):
            print(" x_min = ", b)
            x_min = b
        if(x_min == 1):
            print(" x_min = ", c)
            x_min = c
        if(x_min == 2):
            print(" x_min = ", d)
            x_min = d
        print(" y_min = ", y_min)
        t = 1
        break
    else:
        # step 6
        if(x_min == 0):
            a = a
            b = c
            y_i[3] = y_i[2]
            tt = 2
            ttt = 0
            
        if(x_min == 3):
            a = d
            b = b
            y_i[0] = y_i[2]
            tt = 2
            ttt = 0
            
        if(x_min == 1):
            a = a
            b = d
            d = c
            c = a + (b - d)
            y_i[3] = y_i[2]
            y_i[2] = y_i[1]
            y_i[1] = 15*(2.25 - np.sin(c))/(np.cos(c))
            N = N + 1
            ttt = 4
            tt = 0
            
        if(x_min == 2):
            a = c
            b = b
            c = d
            d = b - (c - a)
            y_i[0] = y_i[1]
            y_i[1] = y_i[2]
            y_i[2] = 15*(2.25 - np.sin(d))/(np.cos(d))
            N = N + 1
            ttt = 4
            tt = 0
    n = n + 1
    print(n, '--', a, '--', y_i[0], '--', c, '--', y_i[1], '--',d, '--', y_i[2], '--', b, '--', y_i[3], '--', N)
    N = 0
    plt.plot([a, a, b, b], [0, 200-n*10, 200-n*10, 0])
#####################
t2 = time.time() - t1
print(t2, " seconds")
plt.scatter(x_min, y_min, c = 'black')
plt.text(x_min, y_min + 5, "Точка минимума")
plt.title("График целевой функции с интервалами поиска экстремума для метода золотого сечения")
plt.show()


#                                   построение графика функции
x = np.arange(0, np.pi/2, 0.01*np.pi)
y = np.zeros(len(x))
i = 0
for i in range(len(x)-1):
    y[i] = 15*(2.25 - math.sin(x[i]))/(math.cos(x[i]))
plt.plot(x[0:len(x)-1],y[0:len(x)-1], color = 'green')

t1 = time.time()  
###################
#                                   метод парабол
f_i = np.zeros((3, 1))
x_i = np.zeros(3)
i = 0
j = 0
f_min = 0
x_min = 0
a = np.zeros(3)
m = np.zeros((3,3))
x_e = 0
f_e = 0
flag = 0
x = 0
e = 0.01
n = 0
N = 0

# step 1
while((x_i[0] >= x_i[1]) or (x_i[1] >= x_i[2]) or (f_i[0] <= f_i[1]) or (f_i[1] >= f_i[2])):
    print("hello\n")
    x_i[0] = np.random.uniform(0, np.pi/6)
    x_i[1] = np.random.uniform(np.pi/6, np.pi/3)
    x_i[2] = np.random.uniform(np.pi/3, np.pi/2)
    for i in range(len(f_i)):
        f_i[i] = 15*(2.25 - np.sin(x_i[i]))/(np.cos(x_i[i]))
    
N = N + 3  
print(x_i)
print(f_i)

while(flag < 1):
    plt.plot([x_i[0], x_i[1], x_i[2]], [f_i[0], f_i[1], f_i[2]])
    # step 2
    print("step 2")
    i = 0
    for i in range(len(a)):
        for j in range(len(a)):
            m[i][j] = x_i[i] ** (len(a)-1-j)
    print(m, "\n")
    m = np.linalg.inv(m)
    print(m, "\n")
    
    a = m.dot(f_i)
    print(a, "\n")
    
    # step 3
    print("step 3")
    x_e = -a[1]/(2*a[0])
    print(x_e)
    f_e = 15*(2.25 - np.sin(x_e))/(np.cos(x_e))
    N = N + 1
    print(f_e, '\n')
    
    # step 4
    print("step 4")
    print(f_i)
    f_min = np.min(f_i)
    print(f_min)
    print(f_e)
    

    if(f_min > f_e):
            f_min = f_e
            
    print(f_min)
    
    i = 0
    for i in range(len(f_i)):
        if(f_min == f_i[i]):
            x = x_i[i]
        else:
            x = x_e
    
    # step 5
    print("step 5")
    if (abs(x_i[1] - x_e) < e):
        x_min = x_e
        flag = 1
    else:
        # step 6
        print("step 6")
        if((x == x_i[1]) and (x_e < x_i[1])):
            print(1)
            x_i[0] = x_e
            f_i[0] = f_e

        if((x == x_i[1]) and (x_e > x_i[1])):
            print(2)
            x_i[2] = x_e
            f_i[2] = f_e

        if((x == x_e) and (x_e < x_i[1])):
            print(3)
            x_i[2] = x_i[1]
            x_i[1] = x_e
            f_i[2] = f_i[1]
            f_i[1] = f_e

        if((x == x_e) and (x_e > x_i[1])):
            print(4)
            x_i[0] = x_i[1]
            x_i[1] = x_e
            f_i[0] = f_i[1]
            f_i[1] = f_e

    n = n + 1
    print(n, '--', x_i[0], '--', f_i[0], '--', x_i[1], '--', f_i[1], '--', x_i[2], '--', f_i[2], '--', x_e, '--', f_e,'--', N, '\n')
    N = 0
    

print(x_min)
print(f_min)
#####################
t2 = time.time() - t1
print(t2, " seconds")
plt.scatter(x_min, f_min, c = 'black')
plt.text(x_min, f_min + 5, "Точка минимума")
plt.title("График целевой функции для метода парабол")
plt.show()
