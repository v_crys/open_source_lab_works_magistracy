# -*- coding: utf-8 -*-
"""
Created on Thu Sep  3 16:47:34 2020
"""
import math
from array import *

#Условия задачи
b=2.5
H=20
V=1
e=0.01 #точность

#Входные данные- границы интервала, C,D,E-середины
dataAngle= array('f', [math.pi/6, 0, 0, 0 , 0])              #A,B,C,D,E
data= array('f', [0, 0, 0, 0 , 0])                           #A,B,C,D,E


while (dataAngle[1]-dataAngle[0])<e:
    #Шаг 1 - определение точек
    dataAngle[2]=dataAngle[0]+((dataAngle[1]-dataAngle[0])/2)    #С=A+((B-A)/2)
    dataAngle[3]=dataAngle[0]+((dataAngle[2]-dataAngle[0])/2)    #D=A+((C-A)/2)
    dataAngle[4]=dataAngle[2]+((dataAngle[1]-dataAngle[2])/2)    #E=C+((B-C)/2)
    
    #Шаг 2 - значение функции 
    #Целевая функция L=(b*V-V*math.sin(a))*H/(V*math.cos(a))
    for i in range(len(dataAngle)):
        data[i]=(b*V-V*math.sin(dataAngle[i]))*H/(V*math.cos(dataAngle[i]))
       # data[i]=FuncL(dataAngle[i],V,H,b)
    
    #Шаг 3 - поиск минимума
    minim=0.0
    minim=min(data)
        
    for i in range(len(data)):
           if data[i]==minim: mini=i
            
    if mini==0:
        dataAngle[1]=dataAngle[3]
        data[1]=data[3]
    elif mini==1:
        dataAngle[0]=dataAngle[4]
        data[0]=data[4]
    elif mini==2:
        dataAngle[0]=dataAngle[3]
        data[0]=data[3]
        dataAngle[1]=dataAngle[4]
        data[1]=data[4]
    elif mini==3:
        dataAngle[1]=dataAngle[2]
        data[1]=data[2]
        dataAngle[2]=dataAngle[3]
        data[2]=data[3]
    elif mini==4:
        dataAngle[0]=dataAngle[3]
        data[0]=data[3]
        dataAngle[3]=dataAngle[4]
        data[3]=data[4]
    
    
print("L=",minim," a=",dataAngle[mini])



#Вычисление координат центра интервала AB=C; центра отрезков AC=D, CB=E
#Нахождение значений целевой функции в точке минимума для каждой точки