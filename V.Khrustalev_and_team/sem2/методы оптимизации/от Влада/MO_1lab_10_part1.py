# -*- coding: utf-8 -*-
"""
Created on Thu Sep  3 16:47:34 2020
"""
import math
from array import *

#Условия задачи
b=2.5
H=20
V=1
e=0.01 #точность

#Входные данные- границы интервала, C,D,E-середины
dataAngle= array('f', [0, math.pi/6, 0, 0 , 0])              #A,B,C,D,E
data= array('f', [0, 0, 0, 0 , 0])                           #A,B,C,D,E
iterator=0
minim=0.0
mini=0

while (dataAngle[1]-dataAngle[0])>e: #Проверка на точность
    print(iterator)
    iterator+=1
   
    #Шаг 1 - определение точек
    dataAngle[2]=dataAngle[0]+((dataAngle[1]-dataAngle[0])/2)    #С=A+((B-A)/2)
    dataAngle[3]=dataAngle[0]+((dataAngle[2]-dataAngle[0])/2)    #D=A+((C-A)/2)
    dataAngle[4]=dataAngle[2]+((dataAngle[1]-dataAngle[2])/2)    #E=C+((B-C)/2)
    
    #Шаг 2 - значение функции 
    #Целевая функция L=(b*V-V*math.sin(a))*H/(V*math.cos(a))
    for i in range(len(dataAngle)):
        data[i]=((b*V-V*math.sin(dataAngle[i]))*H)/(V*math.cos(dataAngle[i]))
       # data[i]=FuncL(dataAngle[i],V,H,b)
    
    #Шаг 3 - поиск минимума
    minim=min(data) #получение значения минимума  
    
    #Шаг 4 - смена значений согласно последнему шагу алгоритма (см.Шаг 8 в документации)
    for i in range(len(data)): #определение буквы минимума
           if data[i]==minim: mini=i

    if mini==0:                     #A
        dataAngle[1]=dataAngle[3]   #B=D
        data[1]=data[3]
    elif mini==1:                   #B
        dataAngle[0]=dataAngle[4]   #A=E
        data[0]=data[4]
    elif mini==2:                   #C
        dataAngle[0]=dataAngle[3]   #A=D
        data[0]=data[3]
        dataAngle[1]=dataAngle[4]   #B=E
        data[1]=data[4]
    elif mini==3:                   #D
        dataAngle[1]=dataAngle[2]   #B=C
        data[1]=data[2]
        dataAngle[2]=dataAngle[3]   #C=D
        data[2]=data[3]
    elif mini==4:                   #E
        dataAngle[0]=dataAngle[3]   #A=C
        data[0]=data[3]
        dataAngle[3]=dataAngle[4]   #C=E
        data[3]=data[4]
    
print("L=",minim," a=",dataAngle[mini])
print("A=",dataAngle[0]," B=",dataAngle[1])

#Вычисление координат центра интервала AB=C; центра отрезков AC=D, CB=E
#Нахождение значений целевой функции в точке минимума для каждой точки