# -*- coding: utf-8 -*-
"""
Created on Wed May 20 11:47:25 2020

@author: 79117
"""


import cv2
import numpy as np
import math as mt
from PIL import Image

for k in range(0, 3):
    #           2.2
    #       image
    print(k + 1, ' image processing...')
    if(k == 0):
        img1 = Image.open('image_1_7.jpg')
    if(k == 1):
        img1 = Image.open('image_2_7.jpg')
    if(k == 2):
        img1 = Image.open('image_3_7.jpg')
    if(k == 3):
        img1 = Image.open('image_4_7.jpg')
    
    imgf = img1.convert('L')
    im = np.asarray(imgf, dtype = np.uint8)
    if(k == 0):
        c = "Image 1"
    if(k == 1):
        c = "Image 2"
    if(k == 2):
        c = "Image 3"
    if(k == 3):
        c = "Image 4"
    cv2.namedWindow(c, cv2.WINDOW_NORMAL)
    cv2.imshow(c, im)

    #img11 = img1.convert('L')
    img111 = np.asarray(imgf, dtype = 'uint8')
    #
    imm = Image.open("cherny.jpg")
    imm_2 = imm.convert('L')
    ImTrf = imm_2.copy()
    ImTrf_im = np.asarray(ImTrf, dtype = np.uint8)
    ImTrf_im = np.reshape(ImTrf_im , -1)
    ImTrf_im.setflags(1,0,0)
    #print("img = ", ImTrf_im)
    #
    XSize, YSize = img1.size
    #print("XSize = ", XSize)
    mass1  = [XSize * YSize]

    #ImChar = np.reshape(img111 , -1)

    summ_x_t = 0
    summ_t = 0
    summ_y_t = 0
    B = 0
    C = 0
    D = 0

    mass1 = img111
    #print("len(mass) = ", len(mass1))
    mass1 = np.reshape(img111 , -1)
    #print("mass = ", mass1)
    #print("len(mass) = ", len(mass1))
    
    #Вычисление абсциссы и ординаты центра тяжести изображения
    for i in range(0, XSize):
        for j in range(0, YSize):
            summ_x_t += j * mass1[i * XSize + j]
            summ_t += mass1[i * XSize + j]
            summ_y_t += i * mass1[i * XSize + j]
    xc = summ_x_t / summ_t
    yc = summ_y_t / summ_t
    print("xc = ",xc , "yc = ", yc)

    #Вычисление направления и величины сжатия изображения
    for i in range(0, XSize):
        for j in range(0, YSize):
            B += mass1[i * XSize + j] * (((j - xc) * (j - xc)) - ((i - yc) * (i - yc)))
            C += mass1[i * XSize + j] * 2 * (j - xc) * (i - yc)
            D += mass1[i * XSize + j] * (((j - xc) * (j - xc)) + ((i - yc) * (i - yc)))
    print("B = ", B, "C = ", C, "D = ", D)

    #Величина сжатия изображения
    mu = mt.sqrt((D + mt.sqrt((C * C) + (B * B))) / (D - mt.sqrt((C * C) + (B * B))))

    #Направление сжатия изображения
    teta = (0.5 * mt.atan2(C, B))
    print("mu = ", mu, "teta = ", teta)

    x_pls = 0
    y_pls = 0
    summ1 = 0
    summ2 = 0

    #Расчёт абциссы и ординаты пикселов центрированного
    #изображения после компенсации его масштабирования
    for i in range(0,YSize):
        for j in range(0, XSize):
            x_pls = (1 / mu) * ((j - xc) * mt.cos(-teta) - (i - yc) * mt.sin(-teta)) * mt.cos(teta) - ((j - xc) * mt.sin(-teta) + (i - yc) * mt.cos(-teta)) * mt.sin(teta)
            y_pls = (1 / mu) * ((j - xc) * mt.cos(-teta) - (i - yc) * mt.sin(-teta)) * mt.sin(teta) + ((j - xc) * mt.sin(-teta) + (i - yc) * mt.cos(-teta)) * mt.cos(teta)
            
            summ1 += mass1[i * XSize + j] * mt.sqrt((x_pls * x_pls) + (y_pls * y_pls))
            summ2 += mass1[i * XSize + j]

    #           2.3
    K = 10
    #Величина равномерного масштабирования изображения
    M = summ1 / (K * summ2)
    print("M = ", M)
    for i in range(0, YSize):
        for j in range(0, XSize):
            x_pls = (1 / mu) * ((j - xc) * mt.cos(-teta) - (i - yc) * mt.sin(-teta)) * mt.cos(teta) - ((j - xc) * mt.sin(-teta) + (i - yc) * mt.cos(-teta)) * mt.sin(teta)
            y_pls = (1 / mu) * ((j - xc) * mt.cos(-teta) - (i - yc) * mt.sin(-teta)) * mt.sin(teta) + ((j - xc) * mt.sin(-teta) + (i - yc) * mt.cos(-teta)) * mt.cos(teta)

            x = (1 / M) * x_pls
            y = (1 / M) * y_pls
            xi = x + xc
            yi = y + yc
            x = int(x)
            y = int(y)
            xi = int(x + XSize / 2)
            yi = int(y + YSize / 2)
            #Центрируем в обратную сторону, чтобы изображ было в центре
            if (xi > 0) and (yi > 0) and (xi <= XSize) and (yi <= YSize):
                ImTrf_im[yi * XSize + xi] = mass1[i * XSize + j]

    ImTrf_im = ImTrf_im.reshape(XSize, YSize)
    
    if(k == 0):
        cc = "Image 1 (result)"
    if(k == 1):
        cc = "Image 2 (result)"
    if(k == 2):
        cc = "Image 3 (result)"
    if(k == 3):
        cc = "Image 4 (result)"
    cv2.namedWindow(cc, cv2.WINDOW_NORMAL)
    cv2.imshow(cc, ImTrf_im)

    ImTrf_im = cv2.resize(ImTrf_im,(0, 0), fx = 5, fy = 5)

    if(k == 0):
        cv2.imwrite('RESULT1.jpg', ImTrf_im)
    if(k == 1):
        cv2.imwrite('RESULT2.jpg', ImTrf_im)
    if(k == 2):
        cv2.imwrite('RESULT3.jpg', ImTrf_im)
    if(k == 3):
        cv2.imwrite('RESULT4.jpg', ImTrf_im)

    #           2.4
    #определение азимутальной координаты пиксела
    #XSize = len(ImTrf_im)
    #YSize = len(ImTrf_im[0])
    #coord = np.zeros((XSize, YSize))
    
    #for i in range(0, XSize):
        #for j in range(0, YSize):
            #coord[i][j] = mt.atan2(i - XSize/2, j - XSize/2)
    
    #           2.5
    #сектора
    #diap = np.arange(0, 2 * np.pi, 2 * np.pi/64)
    
    #подсчет суммы пикселов всего изображения
    #summ_all = 0
    #for i in range(0, XSize):
        #for j in range(0, YSize):
            #summ_all += ImTrf_im[i][j]
    #print(summ_all)
    
    #подсчет суммы для каждого сектора
    #summ_sector = np.zeros((64, 1))
    #for i in range(0, XSize):
        #for j in range(0, YSize):
            #for n in range(0, 64):
                #if(n != 63):
                    #if((diap[n + 1] > coord[i][j]) and (diap[n] <= coord[i][j])):
                        #summ_sector[n] += ImTrf_im[i][j]
                #else:
                    #if(diap[n] <= coord[i][j]):
                        #summ_sector[n] += ImTrf_im[i][j]

    #формирование вектора признаков
    #summ_sector = summ_sector / summ_all
    
    #           2.6
    #БПФ
    #fft = np.fft2(summ_sector)
    
    #cv2.waitKey(0)
    #cv2.destroyAllWindows()
