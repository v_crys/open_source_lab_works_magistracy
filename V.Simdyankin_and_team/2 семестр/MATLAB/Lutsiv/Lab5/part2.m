%число секторов
N = 256;

%чтение изображений
for i = 1:4
    switch i
        case 1
            img1 = double(imread('RESULT1.jpg'));
        case 2
            img2 = double(imread('RESULT2.jpg'));
        case 3
            img3 = double(imread('RESULT3.jpg'));
        case 4
            img4 = double(imread('RESULT3.jpg'));
    end
end

%           2.4
%определение азимутальной координаты
XSize = size(img1, 1);
YSize = size(img1, 2);
coord = zeros(XSize, YSize);

for i = 1:XSize
    for j = 1:YSize
        coord(i, j) = atan2(i - XSize/2, j - YSize/2);
    end
end

%           2.5
%сектора
diap = (0:2*pi/N:2*pi)-pi;

%подсчет суммы пикселов всего изображения
summ_all_1 = 0;
summ_all_2 = 0;
summ_all_3 = 0;
summ_all_4 = 0;
for i = 1:XSize
    for j = 1:YSize
        summ_all_1 = summ_all_1 + img1(i, j);
        summ_all_2 = summ_all_2 + img2(i, j);
        summ_all_3 = summ_all_3 + img3(i, j);
        summ_all_4 = summ_all_4 + img4(i, j);
    end
end

%подсчет суммы для каждого сектора
summ_sector_1 = zeros(1, N);
summ_sector_2 = zeros(1, N);
summ_sector_3 = zeros(1, N);
summ_sector_4 = zeros(1, N);
for i = 1:XSize
    for j = 1:YSize
        for n = 1:N
            if((coord(i, j) >= diap(n)) && (coord(i, j) < diap(n+1)))
                summ_sector_1(n) = summ_sector_1(n) + img1(i, j);
                summ_sector_2(n) = summ_sector_2(n) + img2(i, j);
                summ_sector_3(n) = summ_sector_3(n) + img3(i, j);
                summ_sector_4(n) = summ_sector_4(n) + img4(i, j);
            end
        end
    end
end

summ_sector_1 = summ_sector_1 / summ_all_1;
summ_sector_2 = summ_sector_2 / summ_all_2;
summ_sector_3 = summ_sector_3 / summ_all_3;
summ_sector_4 = summ_sector_4 / summ_all_4;

figure, plot(summ_sector_1);
figure, plot(summ_sector_2);
figure, plot(summ_sector_3);
figure, plot(summ_sector_4);

f1 = fft2(summ_sector_1);
f2 = fft2(summ_sector_2);
f3 = fft2(summ_sector_3);
f4 = fft2(summ_sector_4);

af1 = zeros(size(f1));
af2 = zeros(size(f2));
af3 = zeros(size(f3));
af4 = zeros(size(f4));

af1 = abs(f1);
af2 = abs(f2);
af3 = abs(f3);
af4 = abs(f4);

imm11 = [af1; af1];
imm21 = [af2; af1];
imm31 = [af3; af1];
imm41 = [af4; af1];

im11 = pdist(imm11);
im21 = pdist(imm21);
im31 = pdist(imm31);
im41 = pdist(imm41);

graph = [im11 im21 im31];

figure, bar(graph);
xlabel("Number of image");
ylabel("The result of comparing images");
