for i = 1:2
    %������ ��������
    if(i == 1)
        I = rgb2gray(imread('pic.jpg'));
    elseif(i == 2)
        I = rgb2gray(imread('pic4.jpg'));
    end
    figure, imshow(I);
    str = ['Original image #', num2str(i)];
    title(str);
    %�������� ����������
    %���������� �����������
    figure;
    [counts, x] = imhist(I, 100);%100 - ���������� ���������� 
    level = multithresh(I);
    stem(x, counts, '.', 'b');
    str = ['Histogram of image #', num2str(i)];
    title(str);
    hold on 
    plot([level level], [0 max(counts)],'magenta', 'LineWidth', 2);%������� ������������ ������
    s_I = imquantize(I, level);%���������� �����������
    figure, imshow(s_I, []);
    str = ['Binary segmentation of image #', num2str(i), ' by Otsu method'];
    title(str);
    %�������������� ����������
    for j = 2:1:5
        thresh = multithresh(I, j);
        s_I = imquantize(I, thresh);
        rgb = label2rgb(s_I); 
        figure, imshow(rgb);
        str = ['Multithreshold segmentation of image #', num2str(i), ' by Otsu method, M = ', num2str(j+1)];
        title(str);
    end
    t=0;
end

